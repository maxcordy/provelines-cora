/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     CONST = 258,
     TYPE = 259,
     IF = 260,
     DO = 261,
     NAME = 262,
     UNAME = 263,
     PNAME = 264,
     INAME = 265,
     STRING = 266,
     ASSERT = 267,
     PRINT = 268,
     PRINTM = 269,
     C_CODE = 270,
     C_DECL = 271,
     C_EXPR = 272,
     C_STATE = 273,
     C_TRACK = 274,
     RUN = 275,
     LEN = 276,
     ENABLED = 277,
     EVAL = 278,
     PC_VAL = 279,
     TYPEDEF = 280,
     MTYPE = 281,
     INLINE = 282,
     LABEL = 283,
     OF = 284,
     GOTO = 285,
     BREAK = 286,
     ELSE = 287,
     SEMI = 288,
     FI = 289,
     OD = 290,
     SEP = 291,
     ATOMIC = 292,
     NON_ATOMIC = 293,
     D_STEP = 294,
     UNLESS = 295,
     TIMEOUT = 296,
     NONPROGRESS = 297,
     ACTIVE = 298,
     PROCTYPE = 299,
     D_PROCTYPE = 300,
     HIDDEN = 301,
     SHOW = 302,
     ISLOCAL = 303,
     PRIORITY = 304,
     PROVIDED = 305,
     FULL = 306,
     EMPTY = 307,
     NFULL = 308,
     NEMPTY = 309,
     XU = 310,
     CLAIM = 311,
     TRACE = 312,
     INIT = 313,
     WHILE = 314,
     WHEN = 315,
     WAIT = 316,
     RESET = 317,
     SPEC = 318,
     EVENTUALLY = 319,
     ALWAYS = 320,
     GLOBALLY = 321,
     FINALLY = 322,
     ASGN = 323,
     R_RCV = 324,
     RCV = 325,
     O_SND = 326,
     SND = 327,
     OR = 328,
     AND = 329,
     NE = 330,
     EQ = 331,
     LE = 332,
     GE = 333,
     LT = 334,
     GT = 335,
     RSHIFT = 336,
     LSHIFT = 337,
     DECR = 338,
     INCR = 339,
     CONTEXT = 340,
     COUNT = 341,
     NEG = 342,
     UMIN = 343,
     DOT = 344
   };
#endif
/* Tokens.  */
#define CONST 258
#define TYPE 259
#define IF 260
#define DO 261
#define NAME 262
#define UNAME 263
#define PNAME 264
#define INAME 265
#define STRING 266
#define ASSERT 267
#define PRINT 268
#define PRINTM 269
#define C_CODE 270
#define C_DECL 271
#define C_EXPR 272
#define C_STATE 273
#define C_TRACK 274
#define RUN 275
#define LEN 276
#define ENABLED 277
#define EVAL 278
#define PC_VAL 279
#define TYPEDEF 280
#define MTYPE 281
#define INLINE 282
#define LABEL 283
#define OF 284
#define GOTO 285
#define BREAK 286
#define ELSE 287
#define SEMI 288
#define FI 289
#define OD 290
#define SEP 291
#define ATOMIC 292
#define NON_ATOMIC 293
#define D_STEP 294
#define UNLESS 295
#define TIMEOUT 296
#define NONPROGRESS 297
#define ACTIVE 298
#define PROCTYPE 299
#define D_PROCTYPE 300
#define HIDDEN 301
#define SHOW 302
#define ISLOCAL 303
#define PRIORITY 304
#define PROVIDED 305
#define FULL 306
#define EMPTY 307
#define NFULL 308
#define NEMPTY 309
#define XU 310
#define CLAIM 311
#define TRACE 312
#define INIT 313
#define WHILE 314
#define WHEN 315
#define WAIT 316
#define RESET 317
#define SPEC 318
#define EVENTUALLY 319
#define ALWAYS 320
#define GLOBALLY 321
#define FINALLY 322
#define ASGN 323
#define R_RCV 324
#define RCV 325
#define O_SND 326
#define SND 327
#define OR 328
#define AND 329
#define NE 330
#define EQ 331
#define LE 332
#define GE 333
#define LT 334
#define GT 335
#define RSHIFT 336
#define LSHIFT 337
#define DECR 338
#define INCR 339
#define CONTEXT 340
#define COUNT 341
#define NEG 342
#define UMIN 343
#define DOT 344




/* Copy the first part of user declarations.  */
#line 1 "promela.y"


// This is based on the original Yacc grammar of SPIN (spin.y):

/* Copyright (c) 1989-2003 by Lucent Technologies, Bell Laboratories.     */
/* All Rights Reserved.  This software is for educational purposes only.  */
/* No guarantee whatsoever is expressed or implied by the distribution of */
/* this code.  Permission is given to distribute this code provided that  */
/* this introductory message is not removed and no monies are exchanged.  */
/* Software written by Gerard J. Holzmann.  For tool documentation see:   */
/*             http://spinroot.com/                                       */
/* Send all bug-reports and/or questions to: bugs@spinroot.com            */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "main.h"
#include "cnf.h"
#include "list.h"
#include "boolFct.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "tctl.h"
#endif
#include "symbols.h"
#include "automata.h"

// extern - lex
extern int nbrLines;
#ifdef CLOCK
	ptList _clocks = NULL;
	ptTctlFormula tctlProperty = NULL;
#endif



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 45 "promela.y"
{ 
	int       				iVal;
	char*    				sVal;
	tDataTuple				pDataVal;
	ptExpNode				pExpVal;
	ptSymTabNode			pSymTabNodeVal;
	struct fsm_*			pFsmVal;
	struct list_ *			pList;
	struct _tctlFormula_ * 	pTctlFormula;
}
/* Line 193 of yacc.c.  */
#line 322 "promela.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 335 "promela.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  57
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1740

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  109
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  66
/* YYNRULES -- Number of rules.  */
#define YYNRULES  232
/* YYNRULES -- Number of states.  */
#define YYNSTATES  493

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   344

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    90,    77,     2,
      99,   100,    88,    86,   101,    87,   104,    89,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   107,     2,
       2,     2,     2,     2,   108,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   102,     2,   103,    76,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   105,    75,   106,    93,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      78,    79,    80,    81,    82,    83,    84,    85,    91,    92,
      94,    95,    96,    97,    98
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     7,    11,    18,    25,    32,    39,
      41,    45,    49,    53,    57,    61,    65,    69,    73,    77,
      81,    85,    89,    93,    97,   101,   105,   109,   113,   115,
     117,   119,   121,   126,   129,   130,   133,   135,   137,   140,
     142,   144,   146,   148,   150,   152,   154,   156,   158,   168,
     170,   172,   173,   175,   180,   185,   193,   197,   200,   203,
     209,   211,   213,   219,   221,   223,   227,   231,   236,   241,
     243,   245,   247,   252,   254,   258,   260,   263,   267,   271,
     273,   277,   279,   285,   292,   303,   304,   306,   308,   310,
     311,   313,   317,   321,   328,   330,   334,   335,   337,   339,
     343,   345,   349,   351,   355,   359,   367,   369,   373,   378,
     380,   382,   387,   390,   395,   396,   399,   401,   403,   407,
     411,   415,   419,   421,   424,   428,   432,   435,   438,   444,
     449,   454,   457,   459,   463,   469,   475,   479,   481,   483,
     489,   495,   500,   506,   508,   511,   515,   516,   518,   520,
     523,   525,   527,   531,   535,   539,   543,   547,   551,   555,
     559,   563,   567,   571,   575,   579,   583,   587,   591,   595,
     599,   603,   608,   611,   614,   617,   625,   632,   642,   647,
     652,   658,   664,   666,   668,   670,   672,   674,   679,   686,
     693,   697,   701,   702,   705,   707,   709,   710,   715,   717,
     721,   725,   729,   733,   737,   741,   745,   750,   755,   760,
     765,   767,   769,   771,   775,   776,   778,   779,   782,   784,
     789,   791,   795,   797,   802,   804,   807,   809,   813,   818,
     822,   824,   827
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     110,     0,    -1,   118,    -1,   112,    -1,    63,   111,   118,
      -1,    64,    67,    99,   160,   100,   160,    -1,    64,    66,
      99,   160,   100,   160,    -1,    65,    67,    99,   160,   100,
     160,    -1,    65,    66,    99,   160,   100,   160,    -1,   113,
      -1,   112,   101,   113,    -1,    99,   113,   100,    -1,   113,
      86,   113,    -1,   113,    87,   113,    -1,   113,    88,   113,
      -1,   113,    89,   113,    -1,   113,    90,   113,    -1,   113,
      77,   113,    -1,   113,    76,   113,    -1,   113,    75,   113,
      -1,   113,    83,   113,    -1,   113,    82,   113,    -1,   113,
      81,   113,    -1,   113,    80,   113,    -1,   113,    79,   113,
      -1,   113,    78,   113,    -1,   113,    74,   113,    -1,   113,
      73,   113,    -1,   114,    -1,     3,    -1,   116,    -1,     7,
      -1,     7,   102,   113,   103,    -1,   115,   117,    -1,    -1,
     104,   116,    -1,   119,    -1,   120,    -1,   119,   120,    -1,
     121,    -1,   124,    -1,   125,    -1,   126,    -1,   140,    -1,
     127,    -1,   130,    -1,   129,    -1,    33,    -1,   123,   122,
       7,    99,   142,   100,   161,   163,   134,    -1,    44,    -1,
      45,    -1,    -1,    43,    -1,    43,   102,     3,   103,    -1,
      43,   102,     7,   103,    -1,    43,   102,    95,    99,   160,
     100,   103,    -1,    58,   161,   134,    -1,    56,   134,    -1,
      57,   134,    -1,    25,     7,   105,   141,   106,    -1,     7,
      -1,    10,    -1,    27,   128,    99,   168,   100,    -1,   132,
      -1,   131,    -1,    18,    11,    11,    -1,    19,    11,    11,
      -1,    18,    11,    11,    11,    -1,    19,    11,    11,    11,
      -1,    15,    -1,    16,    -1,    17,    -1,   105,   135,   157,
     106,    -1,   136,    -1,   135,   158,   136,    -1,   140,    -1,
      55,   143,    -1,     7,   107,   140,    -1,     7,   107,    55,
      -1,   152,    -1,   152,    40,   152,    -1,   137,    -1,    59,
      99,   160,   100,    61,    -1,    60,    99,   160,   100,     6,
     154,    -1,    60,    99,   160,   100,    62,    99,   144,   100,
       6,   154,    -1,    -1,    46,    -1,    47,    -1,    48,    -1,
      -1,    68,    -1,   138,     4,   144,    -1,   138,     8,   144,
      -1,   138,     4,   139,   105,   174,   106,    -1,   140,    -1,
     140,    33,   141,    -1,    -1,   141,    -1,   148,    -1,   148,
     101,   143,    -1,   145,    -1,   145,   101,   144,    -1,   147,
      -1,   147,    68,   160,    -1,   147,    68,   146,    -1,   102,
       3,   103,    29,   105,   167,   106,    -1,     7,    -1,     7,
     107,     3,    -1,     7,   102,     3,   103,    -1,   150,    -1,
       7,    -1,     7,   102,   160,   103,    -1,   149,   151,    -1,
      94,   104,   149,   151,    -1,    -1,   104,   150,    -1,   153,
      -1,   154,    -1,   148,    70,   173,    -1,   148,    72,   170,
      -1,     5,   155,    34,    -1,     6,   155,    35,    -1,    31,
      -1,    30,     7,    -1,     7,   107,   152,    -1,   148,    68,
     162,    -1,   148,    92,    -1,   148,    91,    -1,    13,    99,
      11,   169,   100,    -1,    14,    99,   148,   100,    -1,    14,
      99,     3,   100,    -1,    12,   162,    -1,   132,    -1,   148,
      69,   173,    -1,   148,    70,    82,   173,    83,    -1,   148,
      69,    82,   173,    83,    -1,   148,    71,   170,    -1,   162,
      -1,    32,    -1,    37,   105,   135,   157,   106,    -1,    39,
     105,   135,   157,   106,    -1,   105,   135,   157,   106,    -1,
      10,    99,   168,   100,   154,    -1,   156,    -1,   156,   155,
      -1,    36,   135,   157,    -1,    -1,    33,    -1,    33,    -1,
     158,    33,    -1,     7,    -1,     9,    -1,    99,   160,   100,
      -1,   160,    86,   160,    -1,   160,    87,   160,    -1,   160,
      88,   160,    -1,   160,    89,   160,    -1,   160,    90,   160,
      -1,   160,    77,   160,    -1,   160,    76,   160,    -1,   160,
      75,   160,    -1,   160,    83,   160,    -1,   160,    82,   160,
      -1,   160,    81,   160,    -1,   160,    80,   160,    -1,   160,
      79,   160,    -1,   160,    78,   160,    -1,   160,    74,   160,
      -1,   160,    73,   160,    -1,   160,    85,   160,    -1,   160,
      84,   160,    -1,    95,    99,   160,   100,    -1,    93,   160,
      -1,    87,   160,    -1,    72,   160,    -1,    99,   160,    33,
     160,   107,   160,   100,    -1,    20,   159,    99,   168,   100,
     161,    -1,    20,   159,   102,   148,   103,    99,   168,   100,
     161,    -1,    21,    99,   148,   100,    -1,    22,    99,   160,
     100,    -1,   148,    70,   102,   173,   103,    -1,   148,    69,
     102,   173,   103,    -1,   148,    -1,   133,    -1,     3,    -1,
      41,    -1,    42,    -1,    24,    99,   160,   100,    -1,     9,
     102,   160,   103,   108,     7,    -1,     9,   102,   160,   103,
     107,   149,    -1,     9,   108,     7,    -1,     9,   107,   149,
      -1,    -1,    49,     3,    -1,   160,    -1,   164,    -1,    -1,
      50,    99,   162,   100,    -1,   165,    -1,    99,   164,   100,
      -1,   164,    74,   164,    -1,   164,    74,   160,    -1,   164,
      73,   164,    -1,   164,    73,   160,    -1,   160,    74,   164,
      -1,   160,    73,   164,    -1,    51,    99,   148,   100,    -1,
      53,    99,   148,   100,    -1,    52,    99,   148,   100,    -1,
      54,    99,   148,   100,    -1,     4,    -1,     8,    -1,   166,
      -1,   166,   101,   167,    -1,    -1,   171,    -1,    -1,   101,
     171,    -1,   171,    -1,   160,    99,   171,   100,    -1,   160,
      -1,   160,   101,   171,    -1,   148,    -1,    23,    99,   160,
     100,    -1,     3,    -1,    87,     3,    -1,   172,    -1,   172,
     101,   173,    -1,   172,    99,   173,   100,    -1,    99,   173,
     100,    -1,     7,    -1,   174,     7,    -1,   174,   101,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   104,   104,   105,   106,   115,   117,   119,   121,   124,
     125,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     149,   152,   156,   159,   166,   167,   173,   180,   181,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   196,   206,
     207,   210,   211,   212,   213,   222,   229,   237,   245,   248,
     278,   279,   282,   285,   286,   289,   290,   291,   292,   295,
     296,   299,   302,   305,   316,   329,   330,   331,   332,   333,
     334,   335,   343,   344,   345,   348,   349,   350,   351,   354,
     355,   361,   376,   389,   395,   396,   400,   401,   404,   405,
     408,   409,   412,   413,   421,   424,   427,   428,   429,   432,
     435,   436,   439,   440,   443,   444,   447,   448,   451,   468,
     469,   470,   471,   472,   473,   477,   490,   491,   492,   493,
     494,   495,   496,   497,   498,   499,   500,   501,   502,   503,
     504,   505,   506,   509,   510,   513,   516,   517,   520,   521,
     524,   525,   528,   529,   530,   531,   532,   533,   534,   535,
     536,   537,   538,   539,   540,   541,   542,   543,   544,   545,
     546,   547,   548,   549,   555,   556,   557,   558,   560,   561,
     562,   563,   564,   565,   566,   567,   568,   569,   570,   571,
     572,   573,   576,   577,   580,   581,   584,   585,   590,   591,
     592,   593,   594,   595,   596,   597,   599,   600,   601,   602,
     605,   606,   610,   618,   630,   631,   635,   636,   640,   641,
     644,   645,   648,   649,   650,   651,   655,   656,   657,   658,
     661,   662,   663
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "CONST", "TYPE", "IF", "DO", "NAME",
  "UNAME", "PNAME", "INAME", "STRING", "ASSERT", "PRINT", "PRINTM",
  "C_CODE", "C_DECL", "C_EXPR", "C_STATE", "C_TRACK", "RUN", "LEN",
  "ENABLED", "EVAL", "PC_VAL", "TYPEDEF", "MTYPE", "INLINE", "LABEL", "OF",
  "GOTO", "BREAK", "ELSE", "SEMI", "FI", "OD", "SEP", "ATOMIC",
  "NON_ATOMIC", "D_STEP", "UNLESS", "TIMEOUT", "NONPROGRESS", "ACTIVE",
  "PROCTYPE", "D_PROCTYPE", "HIDDEN", "SHOW", "ISLOCAL", "PRIORITY",
  "PROVIDED", "FULL", "EMPTY", "NFULL", "NEMPTY", "XU", "CLAIM", "TRACE",
  "INIT", "WHILE", "WHEN", "WAIT", "RESET", "SPEC", "EVENTUALLY", "ALWAYS",
  "GLOBALLY", "FINALLY", "ASGN", "R_RCV", "RCV", "O_SND", "SND", "OR",
  "AND", "'|'", "'^'", "'&'", "NE", "EQ", "LE", "GE", "LT", "GT", "RSHIFT",
  "LSHIFT", "'+'", "'-'", "'*'", "'/'", "'%'", "DECR", "INCR", "'~'",
  "CONTEXT", "COUNT", "NEG", "UMIN", "DOT", "'('", "')'", "','", "'['",
  "']'", "'.'", "'{'", "'}'", "':'", "'@'", "$accept", "start_parsing",
  "tctl", "props", "prop", "varref_", "pfld_", "cmpnd_", "sfld_",
  "program", "units", "unit", "proc", "proctype", "inst", "init", "claim",
  "events", "utype", "nm", "ns", "c_fcts", "cstate", "ccode", "cexpr",
  "body", "sequence", "step", "timed_stmnt", "vis", "asgn", "one_decl",
  "decl_lst", "decl", "vref_lst", "var_list", "ivar", "ch_init", "vardcl",
  "varref", "pfld", "cmpnd", "sfld", "stmnt", "Special", "Stmnt",
  "options", "option", "OS", "MS", "aname", "expr", "Opt_priority",
  "full_expr", "Opt_enabler", "Expr", "Probe", "basetype", "typ_list",
  "args", "prargs", "margs", "arg", "rarg", "rargs", "nlst", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   124,    94,    38,   330,   331,
     332,   333,   334,   335,   336,   337,    43,    45,    42,    47,
      37,   338,   339,   126,   340,   341,   342,   343,   344,    40,
      41,    44,    91,    93,    46,   123,   125,    58,    64
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   109,   110,   110,   110,   111,   111,   111,   111,   112,
     112,   113,   113,   113,   113,   113,   113,   113,   113,   113,
     113,   113,   113,   113,   113,   113,   113,   113,   113,   113,
     114,   115,   115,   116,   117,   117,   118,   119,   119,   120,
     120,   120,   120,   120,   120,   120,   120,   120,   121,   122,
     122,   123,   123,   123,   123,   123,   124,   125,   126,   127,
     128,   128,   129,   130,   130,   131,   131,   131,   131,   132,
     132,   133,   134,   135,   135,   136,   136,   136,   136,   136,
     136,   136,   137,   137,   137,   138,   138,   138,   138,   139,
     139,   140,   140,   140,   141,   141,   142,   142,   143,   143,
     144,   144,   145,   145,   145,   146,   147,   147,   147,   148,
     149,   149,   150,   150,   151,   151,   152,   152,   153,   153,
     153,   153,   153,   153,   153,   154,   154,   154,   154,   154,
     154,   154,   154,   154,   154,   154,   154,   154,   154,   154,
     154,   154,   154,   155,   155,   156,   157,   157,   158,   158,
     159,   159,   160,   160,   160,   160,   160,   160,   160,   160,
     160,   160,   160,   160,   160,   160,   160,   160,   160,   160,
     160,   160,   160,   160,   160,   160,   160,   160,   160,   160,
     160,   160,   160,   160,   160,   160,   160,   160,   160,   160,
     160,   160,   161,   161,   162,   162,   163,   163,   164,   164,
     164,   164,   164,   164,   164,   164,   165,   165,   165,   165,
     166,   166,   167,   167,   168,   168,   169,   169,   170,   170,
     171,   171,   172,   172,   172,   172,   173,   173,   173,   173,
     174,   174,   174
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     3,     6,     6,     6,     6,     1,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     1,     1,
       1,     1,     4,     2,     0,     2,     1,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     9,     1,
       1,     0,     1,     4,     4,     7,     3,     2,     2,     5,
       1,     1,     5,     1,     1,     3,     3,     4,     4,     1,
       1,     1,     4,     1,     3,     1,     2,     3,     3,     1,
       3,     1,     5,     6,    10,     0,     1,     1,     1,     0,
       1,     3,     3,     6,     1,     3,     0,     1,     1,     3,
       1,     3,     1,     3,     3,     7,     1,     3,     4,     1,
       1,     4,     2,     4,     0,     2,     1,     1,     3,     3,
       3,     3,     1,     2,     3,     3,     2,     2,     5,     4,
       4,     2,     1,     3,     5,     5,     3,     1,     1,     5,
       5,     4,     5,     1,     2,     3,     0,     1,     1,     2,
       1,     1,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     4,     2,     2,     2,     7,     6,     9,     4,     4,
       5,     5,     1,     1,     1,     1,     1,     4,     6,     6,
       3,     3,     0,     2,     1,     1,     0,     4,     1,     3,
       3,     3,     3,     3,     3,     3,     4,     4,     4,     4,
       1,     1,     1,     3,     0,     1,     0,     2,     1,     4,
       1,     3,     1,     4,     1,     2,     1,     3,     4,     3,
       1,     2,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      51,    29,    31,    69,    70,     0,     0,     0,     0,    47,
      52,    86,    87,    88,     0,     0,   192,     0,     0,     0,
       3,     9,    28,    34,    30,     2,    51,    37,    39,     0,
      40,    41,    42,    44,    46,    45,    64,    63,     0,    43,
       0,     0,     0,     0,    60,    61,     0,     0,    85,    57,
      58,     0,     0,     0,     0,    51,     0,     1,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    33,    38,    49,    50,
       0,    89,     0,     0,    65,    66,    85,   214,     0,     0,
       0,   184,     0,     0,   110,     0,     0,     0,     0,     0,
      71,     0,     0,     0,     0,     0,   122,   138,     0,     0,
     185,   186,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    85,   132,   183,   146,    73,
      81,    75,   182,   114,   109,    79,   116,   117,   194,   137,
     195,   198,   193,    56,     0,     0,     0,     0,     4,    11,
      10,    27,    26,    19,    18,    17,    25,    24,    23,    22,
      21,    20,    12,    13,    14,    15,    16,    35,     0,   106,
      90,     0,    91,   100,   102,    92,    32,    67,    68,    94,
       0,   110,     0,   182,   220,     0,   215,    53,    54,     0,
      85,     0,   143,     0,     0,    85,     0,     0,     0,   214,
     131,     0,     0,   150,   151,     0,     0,     0,     0,   123,
      85,    85,     0,     0,     0,     0,    76,    98,     0,     0,
     174,   173,   172,     0,     0,     0,     0,   146,   148,     0,
      85,     0,     0,     0,     0,     0,   127,   126,     0,   112,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    85,     0,     0,     0,     0,
       0,    85,    59,     0,     0,     0,     0,     0,     0,    62,
       0,   146,   120,   144,   121,     0,   110,    78,    77,   124,
       0,   191,   190,     0,   216,     0,     0,   214,     0,     0,
       0,     0,   146,   146,     0,     0,     0,     0,     0,     0,
       0,   114,     0,     0,   152,   199,     0,    72,   149,    74,
     125,   224,     0,     0,     0,     0,     0,   222,   226,   133,
       0,     0,   118,   220,   136,   218,   119,   115,    80,   168,
     205,   167,   204,   160,   159,   158,   166,   165,   164,   163,
     162,   161,   170,   169,   153,   154,   155,   156,   157,   203,
     202,   201,   200,     0,     0,     0,     0,    97,     0,     0,
     107,   230,     0,   101,     0,   104,   103,    95,   168,   167,
     221,     0,   145,   111,     0,     0,     0,     0,     0,   130,
     129,     0,     0,   178,   179,   187,     0,     0,   206,   208,
     207,   209,    99,     0,     0,   113,   171,     0,   141,     0,
       0,   225,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   192,   108,   231,   232,    93,     0,    55,
       0,     0,   182,   142,   217,   128,   192,     0,   139,   140,
      82,     0,     0,     0,     0,   135,   229,   181,     0,   227,
     134,   180,     0,     6,     5,     8,     7,   196,     0,   189,
     188,     0,   176,   214,    83,     0,     0,   223,   228,   219,
       0,     0,     0,     0,     0,   175,     0,    48,     0,   192,
       0,     0,   210,   211,   212,     0,   177,     0,   197,     0,
     105,    84,   213
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    19,    55,    20,    21,    22,    23,    24,    76,    25,
      26,    27,    28,    80,    29,    30,    31,    32,    33,    46,
      34,    35,    36,   126,   127,    49,   128,   129,   130,    38,
     171,   131,   180,   368,   216,   172,   173,   375,   174,   183,
     133,   134,   239,   135,   136,   137,   191,   192,   229,   230,
     205,   138,    52,   139,   471,   140,   141,   484,   485,   185,
     388,   334,   186,   328,   329,   372
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -403
static const yytype_int16 yypact[] =
{
     817,  -403,   -57,  -403,  -403,    61,    96,    49,    47,  -403,
      34,  -403,  -403,  -403,    44,    44,   111,    92,    16,   182,
      98,   556,  -403,    80,  -403,  -403,  1003,  -403,  -403,    79,
    -403,  -403,  -403,  -403,  -403,  -403,  -403,  -403,    83,  -403,
      16,   181,   223,   131,  -403,  -403,   136,    21,   568,  -403,
    -403,   234,    44,   107,   121,  1076,   459,  -403,    16,    16,
      16,    16,    16,    16,    16,    16,    16,    16,    16,    16,
      16,    16,    16,    16,    16,   232,  -403,  -403,  -403,  -403,
     235,    15,   239,   324,   230,   249,   -21,   885,   154,   164,
     173,  -403,   237,   237,   -66,   -18,   175,   869,   183,   188,
    -403,    86,   189,   190,   191,   273,  -403,  -403,   186,   215,
    -403,  -403,   242,   243,   244,   256,     5,   257,   261,   885,
     885,   885,   177,   263,   869,   568,  -403,  -403,   305,  -403,
    -403,  -403,   152,   241,  -403,   323,  -403,  -403,  1553,  -403,
     117,  -403,  -403,  -403,   266,   267,   271,   272,  -403,  -403,
     556,   652,  1621,   667,   570,   850,   236,   236,   166,   166,
     166,   166,    43,    43,  -403,  -403,  -403,  -403,   274,   -58,
    -403,   270,  -403,   276,   304,  -403,  -403,  -403,  -403,   347,
     275,   281,   885,   128,  1188,   284,  -403,  -403,  -403,   885,
     568,   352,   237,   357,   885,   665,   885,   380,   386,   885,
    -403,   384,     7,  -403,  -403,   -14,     5,   885,   885,  -403,
     568,   568,     5,     5,     5,     5,  -403,   295,   885,   885,
    -403,  -403,  -403,   380,   885,   129,     0,   305,    45,   302,
     471,   869,    48,    59,   885,   885,  -403,  -403,     5,  -403,
     762,   869,   869,   885,   885,   885,   885,   885,   885,   885,
     885,   885,   885,   885,   885,   885,   885,   885,   885,   869,
     869,   885,   885,   885,   885,   130,   412,   413,   410,   239,
      26,   -21,  -403,   912,   316,   317,   885,   885,   885,  -403,
    1217,   305,  -403,  -403,  -403,  1097,   -32,  -403,  -403,  -403,
    1128,  -403,  -403,   320,   321,   325,   326,   885,     5,   328,
    1245,  1273,   305,   305,   329,   333,   334,   335,     5,  1301,
    1329,   241,  1357,   885,  -403,  -403,   315,  -403,  -403,  -403,
    -403,  -403,   338,    35,   420,    35,    35,  -403,    53,  -403,
      35,    35,  -403,  1159,  -403,  -403,  -403,  -403,  -403,  1588,
     367,  1078,  -403,  1636,  1650,   365,  1108,  1108,   246,   246,
     246,   246,   176,   176,    91,    91,  -403,  -403,  -403,  1588,
     367,  1078,  -403,  1385,  1413,  1441,  1469,  -403,   339,   354,
    -403,  -403,    11,  -403,   455,  -403,  1571,  -403,  1605,  1078,
    -403,   356,  -403,  -403,   762,   124,   337,   885,   361,  -403,
    -403,   362,   360,  -403,  -403,  -403,   358,   359,  -403,  -403,
    -403,  -403,  -403,   405,    24,  -403,  -403,  1062,  -403,   885,
     385,  -403,   375,   364,    35,    35,   396,   379,   885,   885,
     885,   885,   885,   111,  -403,  -403,  -403,  -403,   387,  -403,
     380,   482,   157,  -403,  -403,  -403,   111,   397,  -403,  -403,
    -403,   337,   398,   885,  1497,  -403,  -403,  -403,   399,  -403,
    -403,  -403,   400,  1571,  1571,  1571,  1571,   448,   476,  -403,
    -403,   -62,  -403,   885,  -403,   239,  1525,  -403,  -403,  -403,
     407,    44,   402,   409,   411,  -403,   869,  -403,   100,   111,
     509,   416,  -403,  -403,   419,   415,  -403,   337,  -403,   100,
    -403,  -403,  -403
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -403,  -403,  -403,  -403,  1005,  -403,  -403,   452,  -403,   473,
    -403,   503,  -403,  -403,  -403,  -403,  -403,  -403,  -403,  -403,
    -403,  -403,  -403,    39,  -403,   -15,  -114,   314,  -403,  -403,
    -403,     6,  -212,  -403,   245,   -80,  -403,  -403,  -403,   -47,
    -192,   312,   240,  -188,  -403,  -378,   -78,  -403,  -163,  -403,
    -403,    51,  -402,   -94,  -403,  -115,  -403,  -403,    63,  -195,
    -403,   319,  -218,  -403,  -220,  -403
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -148
static const yytype_int16 yytable[] =
{
      50,   132,   175,   200,   293,   291,    39,   289,   433,   226,
     295,   227,   181,   332,   181,   193,   335,   335,   425,     1,
     330,   457,   169,     2,    88,    11,    12,    13,    89,    91,
     441,   311,    39,   181,   462,    95,   194,   143,   321,    37,
     331,   195,   181,   100,   266,    40,   101,   102,   103,   267,
     104,   321,   338,   367,    44,   181,    43,    45,   322,   377,
     380,    39,   321,   464,   316,    37,   181,   110,   111,   217,
     194,   322,    41,   259,   260,   384,   281,   486,   132,  -147,
    -147,  -147,   322,   170,   196,   297,   442,    81,   298,   197,
     198,    82,   179,   203,    37,   204,   302,   303,   119,   122,
     315,   122,   391,   410,   482,   412,   413,    42,   483,   491,
     416,   417,   426,   120,   283,    18,    90,   427,   382,   121,
     122,   123,   324,    78,    79,   182,   340,   342,   374,   122,
     323,    72,    73,    74,   325,   324,    47,   320,   184,   396,
     397,   330,   122,   132,   360,   362,   324,   325,   132,    48,
     326,  -147,   414,   122,   415,   296,    53,    54,   325,   299,
      51,   331,   313,   132,   132,   304,   305,   306,   307,   434,
     220,   221,   222,   144,   145,   225,    11,    12,    13,   256,
     257,   258,    57,   132,    75,   327,   327,   146,   147,   373,
     259,   260,    84,   132,   448,   449,   289,   274,   275,    58,
     452,   288,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,   254,   255,   256,   257,   258,
     231,   232,   233,   234,   235,   231,   232,   461,   234,   314,
     -96,   430,   431,   273,    85,    87,    86,   142,   459,     2,
     280,   177,   168,   236,   237,   285,   169,   290,   236,   237,
     184,   392,    70,    71,    72,    73,    74,   187,   300,   301,
     178,   217,   254,   255,   256,   257,   258,   188,   473,   309,
     310,   179,   189,   190,   199,   312,   327,   179,   327,   327,
     209,   223,   201,   327,   327,   333,   333,   202,   206,   207,
     208,   210,   339,   341,   343,   344,   345,   346,   347,   348,
     349,   350,   351,   352,   353,   354,   355,   356,   357,   358,
     359,   361,   363,   364,   365,   366,    66,    67,    68,    69,
     211,   376,    70,    71,    72,    73,    74,   378,   379,   184,
     252,   253,   254,   255,   256,   257,   258,   132,   228,   432,
      91,   212,   213,   214,   181,   238,    95,    96,   184,    97,
      98,    99,     3,     4,   100,   215,   218,   101,   102,   103,
     219,   104,   224,   240,   407,   261,   262,   327,   327,   107,
     263,   264,   270,   265,   108,   268,   109,   269,   110,   111,
     271,   272,   481,   194,   279,   474,   282,   181,   112,   113,
     114,   115,   284,   292,   432,   294,   308,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,   317,   119,
      70,    71,    72,    73,    74,   369,   370,   371,   326,   331,
     386,   408,   387,   411,   120,   389,   390,   176,   393,   398,
     121,   122,   123,   399,   400,   401,   124,   409,   184,   423,
     432,   260,   125,   246,   247,   248,   249,   250,   251,   252,
     253,   254,   255,   256,   257,   258,   477,   424,   428,   429,
     444,   435,   436,   437,   438,   439,   440,   447,   445,   184,
     453,   454,   455,   456,    91,   446,    92,    93,    94,   450,
      95,    96,   451,    97,    98,    99,     3,     4,   100,   460,
     458,   101,   102,   103,   466,   104,   463,   465,   470,   468,
     469,   105,   106,   107,   318,   472,   476,   478,   108,   479,
     109,   480,   110,   111,   184,   487,   488,    11,    12,    13,
     489,   490,   112,   113,   114,   115,   116,   167,   148,    77,
     117,   118,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,   119,   319,    70,    71,    72,    73,    74,
     337,   405,   492,   402,   336,     0,     0,     0,   120,   149,
       0,     0,     0,     0,   121,   122,   123,     0,     0,     0,
     124,    91,     0,    92,    93,    94,   125,    95,    96,     0,
      97,    98,    99,     3,     4,   100,     0,     0,   101,   102,
     103,     0,   104,     0,     0,     0,     0,     0,   105,   106,
     107,     0,     0,     0,     0,   108,     0,   109,     0,   110,
     111,     0,     0,     0,    11,    12,    13,     0,     0,   112,
     113,   114,   115,   116,     0,     0,     0,   117,   118,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
     119,     0,    70,    71,    72,    73,    74,    63,    64,    65,
      66,    67,    68,    69,     0,   120,    70,    71,    72,    73,
      74,   121,   122,   123,     0,     0,     0,   124,    91,     0,
      92,    93,   286,   125,    95,    96,     0,    97,    98,    99,
       3,     4,   100,     0,     0,   101,   102,   103,     0,   104,
       0,     0,     0,     0,     0,   105,   106,   107,     0,     0,
       0,     0,   108,     0,   109,     0,   110,   111,     0,     0,
       0,    11,    12,    13,     0,     0,   112,   113,   114,   115,
     287,     0,     0,     0,     0,     0,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,     0,   119,    70,    71,
      72,    73,    74,    62,    63,    64,    65,    66,    67,    68,
      69,     0,   120,    70,    71,    72,    73,    74,   121,   122,
     123,     0,     0,     0,   124,    91,     0,    92,    93,   286,
     125,    95,    96,     0,    97,    98,    99,     3,     4,   100,
       0,     0,   101,   102,   103,     0,   104,     0,     0,     0,
       0,     0,   105,   106,   107,     0,     0,     0,     0,   108,
       0,   109,     0,   110,   111,     0,     0,     0,     0,     0,
       0,     0,     0,   112,   113,   114,   115,     0,     0,     0,
       1,   -85,     0,     0,     2,   -85,     0,     0,     0,     0,
       0,     0,     3,     4,   119,     5,     6,     0,     0,     0,
       0,     0,     7,     0,     8,     0,     0,     0,     0,   120,
       9,     0,     0,     0,     0,   121,   122,   123,     0,     0,
      10,   124,     0,    11,    12,    13,     0,   125,     0,     0,
       0,     0,    91,    14,    15,    16,   181,     0,    95,     0,
      17,     0,     0,     0,     0,     0,   100,     0,    91,   101,
     102,   103,   181,   104,    95,     0,     0,     0,     0,     0,
       0,     0,   100,     0,     0,   101,   102,   103,     0,   104,
     110,   111,     0,     0,     0,     0,    18,     0,     0,     0,
     112,   113,   114,   115,     0,     0,   110,   111,    64,    65,
      66,    67,    68,    69,     0,     0,    70,    71,    72,    73,
      74,   119,     0,     0,     0,   313,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   120,   119,     0,     0,
       0,     0,   121,   122,   123,     0,     0,     0,   124,     0,
       0,     0,   120,     0,     0,     0,     0,     0,   121,   122,
     123,     0,     0,     0,   182,   276,   277,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,   254,   255,
     256,   257,   258,   -36,     0,     0,     0,   -85,     0,     0,
       0,   -85,   314,     0,     0,     0,     0,     0,     3,     4,
       0,     5,     6,    56,     0,     0,     0,     0,     7,     0,
       8,     0,     0,     0,     0,     0,     9,     0,     0,     0,
       0,     0,     0,     0,     0,    83,    10,     0,     0,    11,
      12,    13,     0,     0,     0,     0,     0,     0,     0,    14,
      15,    16,     0,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     -85,     0,     0,     0,   -85,     0,     0,     0,     0,     0,
       0,     3,     4,     0,     5,     6,     0,     0,     0,     0,
       0,     7,     0,     8,     0,     0,     0,     0,     0,     9,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    10,
       0,     0,    11,    12,    13,     0,     0,     0,     0,     0,
       0,     0,    14,    15,    16,   276,   277,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,   254,   255,
     256,   257,   258,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   258,   443,
     276,   277,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,   248,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   258,     0,
     383,   276,   277,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   258,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   385,   276,   277,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,   254,   255,   256,   257,   258,
       0,     0,     0,     0,     0,     0,     0,     0,   418,     0,
     278,   276,   277,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   258,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   278,
     276,   277,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   381,   276,   277,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,   254,   255,   256,   257,   258,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   394,   276,   277,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   395,   276,   277,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,   254,   255,   256,
     257,   258,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   403,   276,   277,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,   254,   255,   256,   257,   258,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   404,
     276,   277,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   406,   276,   277,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,   254,   255,   256,   257,   258,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   419,   276,   277,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   420,   276,   277,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,   254,   255,   256,
     257,   258,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   421,   276,   277,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,   254,   255,   256,   257,   258,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   422,
     276,   277,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   467,   276,   277,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,   254,   255,   256,   257,   258,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   475,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   276,   277,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,   254,   255,   256,
     257,   258,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   258,   277,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,   254,   255,   256,   257,   258,    61,    62,    63,    64,
      65,    66,    67,    68,    69,     0,     0,    70,    71,    72,
      73,    74,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   254,   255,   256,   257,   258,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258
};

static const yytype_int16 yycheck[] =
{
      15,    48,    82,    97,   199,   197,     0,   195,   386,   124,
       3,   125,     7,   233,     7,    93,   234,   235,     7,     3,
      82,   423,     7,     7,     3,    46,    47,    48,     7,     3,
       6,   223,    26,     7,   436,     9,   102,    52,     3,     0,
     102,   107,     7,    17,   102,   102,    20,    21,    22,   107,
      24,     3,   240,   265,     7,     7,     7,    10,    23,   271,
     278,    55,     3,   441,   227,    26,     7,    41,    42,   116,
     102,    23,    11,    73,    74,   107,   190,   479,   125,    34,
      35,    36,    23,    68,   102,    99,    62,     4,   102,   107,
     108,     8,    86,     7,    55,     9,   210,   211,    72,    94,
     100,    94,   297,   323,     4,   325,   326,    11,     8,   487,
     330,   331,   101,    87,   192,    99,    95,   106,   281,    93,
      94,    95,    87,    44,    45,    99,   241,   242,   102,    94,
      82,    88,    89,    90,    99,    87,   102,   231,    87,   302,
     303,    82,    94,   190,   259,   260,    87,    99,   195,   105,
     102,   106,    99,    94,   101,   202,    64,    65,    99,   206,
      49,   102,    33,   210,   211,   212,   213,   214,   215,   387,
     119,   120,   121,    66,    67,   124,    46,    47,    48,    88,
      89,    90,     0,   230,   104,   232,   233,    66,    67,   269,
      73,    74,    11,   240,   414,   415,   384,    69,    70,   101,
     418,   195,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      68,    69,    70,    71,    72,    68,    69,    70,    71,   100,
     100,   107,   108,   182,    11,    99,   105,     3,   430,     7,
     189,    11,     7,    91,    92,   194,     7,   196,    91,    92,
     199,   298,    86,    87,    88,    89,    90,   103,   207,   208,
      11,   308,    86,    87,    88,    89,    90,   103,   463,   218,
     219,   265,    99,    36,    99,   224,   323,   271,   325,   326,
       7,   104,    99,   330,   331,   234,   235,    99,    99,    99,
      99,   105,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,   254,   255,   256,   257,   258,
     259,   260,   261,   262,   263,   264,    80,    81,    82,    83,
     105,   270,    86,    87,    88,    89,    90,   276,   277,   278,
      84,    85,    86,    87,    88,    89,    90,   384,    33,   386,
       3,    99,    99,    99,     7,   104,     9,    10,   297,    12,
      13,    14,    15,    16,    17,    99,    99,    20,    21,    22,
      99,    24,    99,    40,   313,    99,    99,   414,   415,    32,
      99,    99,    68,    99,    37,   105,    39,   101,    41,    42,
      33,   106,   476,   102,   100,   465,    34,     7,    51,    52,
      53,    54,    35,     7,   441,    11,   101,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,   106,    72,
      86,    87,    88,    89,    90,     3,     3,     7,   102,   102,
     100,   106,   101,     3,    87,   100,   100,   103,   100,   100,
      93,    94,    95,   100,   100,   100,    99,    99,   387,   100,
     487,    74,   105,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,   471,   103,     3,   103,
     409,   100,   100,   103,   106,   106,    61,   103,    83,   418,
     419,   420,   421,   422,     3,   100,     5,     6,     7,    83,
       9,    10,   103,    12,    13,    14,    15,    16,    17,     7,
     103,    20,    21,    22,   443,    24,    99,    99,    50,   100,
     100,    30,    31,    32,    33,    29,    99,   105,    37,   100,
      39,   100,    41,    42,   463,     6,   100,    46,    47,    48,
     101,   106,    51,    52,    53,    54,    55,    75,    55,    26,
      59,    60,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    72,   230,    86,    87,    88,    89,    90,
     238,   311,   489,   308,   235,    -1,    -1,    -1,    87,   100,
      -1,    -1,    -1,    -1,    93,    94,    95,    -1,    -1,    -1,
      99,     3,    -1,     5,     6,     7,   105,     9,    10,    -1,
      12,    13,    14,    15,    16,    17,    -1,    -1,    20,    21,
      22,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      32,    -1,    -1,    -1,    -1,    37,    -1,    39,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    -1,    -1,    51,
      52,    53,    54,    55,    -1,    -1,    -1,    59,    60,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      72,    -1,    86,    87,    88,    89,    90,    77,    78,    79,
      80,    81,    82,    83,    -1,    87,    86,    87,    88,    89,
      90,    93,    94,    95,    -1,    -1,    -1,    99,     3,    -1,
       5,     6,     7,   105,     9,    10,    -1,    12,    13,    14,
      15,    16,    17,    -1,    -1,    20,    21,    22,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    32,    -1,    -1,
      -1,    -1,    37,    -1,    39,    -1,    41,    42,    -1,    -1,
      -1,    46,    47,    48,    -1,    -1,    51,    52,    53,    54,
      55,    -1,    -1,    -1,    -1,    -1,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    -1,    72,    86,    87,
      88,    89,    90,    76,    77,    78,    79,    80,    81,    82,
      83,    -1,    87,    86,    87,    88,    89,    90,    93,    94,
      95,    -1,    -1,    -1,    99,     3,    -1,     5,     6,     7,
     105,     9,    10,    -1,    12,    13,    14,    15,    16,    17,
      -1,    -1,    20,    21,    22,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    32,    -1,    -1,    -1,    -1,    37,
      -1,    39,    -1,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    51,    52,    53,    54,    -1,    -1,    -1,
       3,     4,    -1,    -1,     7,     8,    -1,    -1,    -1,    -1,
      -1,    -1,    15,    16,    72,    18,    19,    -1,    -1,    -1,
      -1,    -1,    25,    -1,    27,    -1,    -1,    -1,    -1,    87,
      33,    -1,    -1,    -1,    -1,    93,    94,    95,    -1,    -1,
      43,    99,    -1,    46,    47,    48,    -1,   105,    -1,    -1,
      -1,    -1,     3,    56,    57,    58,     7,    -1,     9,    -1,
      63,    -1,    -1,    -1,    -1,    -1,    17,    -1,     3,    20,
      21,    22,     7,    24,     9,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    17,    -1,    -1,    20,    21,    22,    -1,    24,
      41,    42,    -1,    -1,    -1,    -1,    99,    -1,    -1,    -1,
      51,    52,    53,    54,    -1,    -1,    41,    42,    78,    79,
      80,    81,    82,    83,    -1,    -1,    86,    87,    88,    89,
      90,    72,    -1,    -1,    -1,    33,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    72,    -1,    -1,
      -1,    -1,    93,    94,    95,    -1,    -1,    -1,    99,    -1,
      -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,    93,    94,
      95,    -1,    -1,    -1,    99,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,     0,    -1,    -1,    -1,     4,    -1,    -1,
      -1,     8,   100,    -1,    -1,    -1,    -1,    -1,    15,    16,
      -1,    18,    19,    18,    -1,    -1,    -1,    -1,    25,    -1,
      27,    -1,    -1,    -1,    -1,    -1,    33,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    40,    43,    -1,    -1,    46,
      47,    48,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    56,
      57,    58,    -1,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
       4,    -1,    -1,    -1,     8,    -1,    -1,    -1,    -1,    -1,
      -1,    15,    16,    -1,    18,    19,    -1,    -1,    -1,    -1,
      -1,    25,    -1,    27,    -1,    -1,    -1,    -1,    -1,    33,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,
      -1,    -1,    46,    47,    48,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    56,    57,    58,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,   107,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    -1,
     103,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   103,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    99,    -1,
     101,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   101,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   100,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   100,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   100,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   100,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   100,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   100,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   100,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   100,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   100,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   100,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   100,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   100,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    -1,    -1,    86,    87,    88,
      89,    90,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     7,    15,    16,    18,    19,    25,    27,    33,
      43,    46,    47,    48,    56,    57,    58,    63,    99,   110,
     112,   113,   114,   115,   116,   118,   119,   120,   121,   123,
     124,   125,   126,   127,   129,   130,   131,   132,   138,   140,
     102,    11,    11,     7,     7,    10,   128,   102,   105,   134,
     134,    49,   161,    64,    65,   111,   113,     0,   101,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      86,    87,    88,    89,    90,   104,   117,   120,    44,    45,
     122,     4,     8,   113,    11,    11,   105,    99,     3,     7,
      95,     3,     5,     6,     7,     9,    10,    12,    13,    14,
      17,    20,    21,    22,    24,    30,    31,    32,    37,    39,
      41,    42,    51,    52,    53,    54,    55,    59,    60,    72,
      87,    93,    94,    95,    99,   105,   132,   133,   135,   136,
     137,   140,   148,   149,   150,   152,   153,   154,   160,   162,
     164,   165,     3,   134,    66,    67,    66,    67,   118,   100,
     113,   113,   113,   113,   113,   113,   113,   113,   113,   113,
     113,   113,   113,   113,   113,   113,   113,   116,     7,     7,
      68,   139,   144,   145,   147,   144,   103,    11,    11,   140,
     141,     7,    99,   148,   160,   168,   171,   103,   103,    99,
      36,   155,   156,   155,   102,   107,   102,   107,   108,    99,
     162,    99,    99,     7,     9,   159,    99,    99,    99,     7,
     105,   105,    99,    99,    99,    99,   143,   148,    99,    99,
     160,   160,   160,   104,    99,   160,   164,   135,    33,   157,
     158,    68,    69,    70,    71,    72,    91,    92,   104,   151,
      40,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    73,
      74,    99,    99,    99,    99,    99,   102,   107,   105,   101,
      68,    33,   106,   160,    69,    70,    73,    74,   101,   100,
     160,   135,    34,   155,    35,   160,     7,    55,   140,   152,
     160,   149,     7,   168,    11,     3,   148,    99,   102,   148,
     160,   160,   135,   135,   148,   148,   148,   148,   101,   160,
     160,   149,   160,    33,   100,   100,   157,   106,    33,   136,
     162,     3,    23,    82,    87,    99,   102,   148,   172,   173,
      82,   102,   173,   160,   170,   171,   170,   150,   152,   160,
     164,   160,   164,   160,   160,   160,   160,   160,   160,   160,
     160,   160,   160,   160,   160,   160,   160,   160,   160,   160,
     164,   160,   164,   160,   160,   160,   160,   141,   142,     3,
       3,     7,   174,   144,   102,   146,   160,   141,   160,   160,
     171,   100,   157,   103,   107,   103,   100,   101,   169,   100,
     100,   168,   148,   100,   100,   100,   157,   157,   100,   100,
     100,   100,   143,   100,   100,   151,   100,   160,   106,    99,
     173,     3,   173,   173,    99,   101,   173,   173,    99,   100,
     100,   100,   100,   100,   103,     7,   101,   106,     3,   103,
     107,   108,   148,   154,   171,   100,   100,   103,   106,   106,
      61,     6,    62,   107,   160,    83,   100,   103,   173,   173,
      83,   103,   171,   160,   160,   160,   160,   161,   103,   149,
       7,    70,   161,    99,   154,    99,   160,   100,   100,   100,
      50,   163,    29,   168,   144,   100,    99,   134,   105,   100,
     100,   162,     4,     8,   166,   167,   161,     6,   100,   101,
     106,   154,   167
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (globalSymTab, mtypes, property, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, yylval, globalSymTab)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, globalSymTab, mtypes, property); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, struct symTabNode_* * globalSymTab, struct mTypeNode_* * mtypes, void ** property)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, globalSymTab, mtypes, property)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    struct symTabNode_* * globalSymTab;
    struct mTypeNode_* * mtypes;
    void ** property;
#endif
{
  if (!yyvaluep)
    return;
  YYUSE (globalSymTab);
  YYUSE (mtypes);
  YYUSE (property);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, struct symTabNode_* * globalSymTab, struct mTypeNode_* * mtypes, void ** property)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, globalSymTab, mtypes, property)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    struct symTabNode_* * globalSymTab;
    struct mTypeNode_* * mtypes;
    void ** property;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep, globalSymTab, mtypes, property);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule, struct symTabNode_* * globalSymTab, struct mTypeNode_* * mtypes, void ** property)
#else
static void
yy_reduce_print (yyvsp, yyrule, globalSymTab, mtypes, property)
    YYSTYPE *yyvsp;
    int yyrule;
    struct symTabNode_* * globalSymTab;
    struct mTypeNode_* * mtypes;
    void ** property;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       , globalSymTab, mtypes, property);
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule, globalSymTab, mtypes, property); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, struct symTabNode_* * globalSymTab, struct mTypeNode_* * mtypes, void ** property)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, globalSymTab, mtypes, property)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    struct symTabNode_* * globalSymTab;
    struct mTypeNode_* * mtypes;
    void ** property;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (globalSymTab);
  YYUSE (mtypes);
  YYUSE (property);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (struct symTabNode_* * globalSymTab, struct mTypeNode_* * mtypes, void ** property);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (struct symTabNode_* * globalSymTab, struct mTypeNode_* * mtypes, void ** property)
#else
int
yyparse (globalSymTab, mtypes, property)
    struct symTabNode_* * globalSymTab;
    struct mTypeNode_* * mtypes;
    void ** property;
#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 105 "promela.y"
    {if(property) *property = (yyvsp[(1) - (1)].pList);;}
    break;

  case 4:
#line 106 "promela.y"
    {
#ifdef CLOCK
													if(property) *property = (yyvsp[(2) - (3)].pTctlFormula);
#else
													failure("[parser/yyparse] Timed CTL formulae can only be checked if CLOCK option is enabled.\n");
#endif
												;}
    break;

  case 5:
#line 115 "promela.y"
    {(yyval.pTctlFormula) = createTctlFormula(CTL_EF, NULL, (yyvsp[(6) - (6)].pExpVal));;}
    break;

  case 6:
#line 117 "promela.y"
    {(yyval.pTctlFormula) = createTctlFormula(CTL_EG, NULL, (yyvsp[(6) - (6)].pExpVal));;}
    break;

  case 7:
#line 119 "promela.y"
    {(yyval.pTctlFormula) = createTctlFormula(CTL_EG, NULL, createExpNode(E_EXPR_NEG, NULL, 0, (yyvsp[(6) - (6)].pExpVal), NULL, NULL, nbrLines, NULL, NULL););;}
    break;

  case 8:
#line 121 "promela.y"
    {(yyval.pTctlFormula) = createTctlFormula(CTL_EF, NULL, createExpNode(E_EXPR_NEG, NULL, 0, (yyvsp[(6) - (6)].pExpVal), NULL, NULL, nbrLines, NULL, NULL););;}
    break;

  case 9:
#line 124 "promela.y"
    {(yyval.pList) = listAdd(NULL,(yyvsp[(1) - (1)].pExpVal));;}
    break;

  case 10:
#line 125 "promela.y"
    {(yyval.pList) = listAdd((yyvsp[(1) - (3)].pList),(yyvsp[(3) - (3)].pExpVal));;}
    break;

  case 11:
#line 128 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_PAR, 	NULL, 	0, (yyvsp[(2) - (3)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 12:
#line 129 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_PLUS, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 13:
#line 130 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_MINUS, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 14:
#line 131 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_TIMES, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 15:
#line 132 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_DIV, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 16:
#line 133 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_MOD, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 17:
#line 134 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_BITWAND,NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 18:
#line 135 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_BITWXOR,NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 19:
#line 136 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_BITWOR,	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 20:
#line 137 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_GT, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 21:
#line 138 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_LT, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 22:
#line 139 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_GE, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 23:
#line 140 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_LE, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 24:
#line 141 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_EQ, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 25:
#line 142 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_NE, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 26:
#line 143 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_AND, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 27:
#line 144 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_OR, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 28:
#line 145 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_VAR, 	NULL, 	0, (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 29:
#line 146 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_CONST,  NULL,(yyvsp[(1) - (1)].iVal), NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 30:
#line 149 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 31:
#line 152 "promela.y"
    { 	ptSymTabNode symbol = lookupInSymTab(*globalSymTab, (yyvsp[(1) - (1)].sVal));
													if(symbol) (yyval.pExpVal) = createExpNode(E_VARREF_NAME, (yyvsp[(1) - (1)].sVal), 0, NULL, NULL, NULL, nbrLines, NULL, symbol);
													else (yyval.pExpVal) = createExpNode(E_EXPR_CONST,  NULL, getMTypeValue(*mtypes, (yyvsp[(1) - (1)].sVal)), NULL, NULL, NULL, nbrLines, NULL, NULL);		
												;}
    break;

  case 32:
#line 156 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_VARREF_NAME,	(yyvsp[(1) - (4)].sVal), 0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 33:
#line 159 "promela.y"
    { if((yyvsp[(1) - (2)].pExpVal)->type == E_VARREF_NAME)
													(yyval.pExpVal) = createExpNode(E_VARREF, NULL, 0,   (yyvsp[(1) - (2)].pExpVal),   (yyvsp[(2) - (2)].pExpVal), NULL, nbrLines, NULL, NULL); 
												  else
												    (yyval.pExpVal) = (yyvsp[(1) - (2)].pExpVal);
												;}
    break;

  case 34:
#line 166 "promela.y"
    { (yyval.pExpVal) = NULL; ;}
    break;

  case 35:
#line 167 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(2) - (2)].pExpVal);   ;}
    break;

  case 36:
#line 173 "promela.y"
    {
#ifdef CLOCK
												  encodeClocks(_clocks);
#endif
												;}
    break;

  case 42:
#line 187 "promela.y"
    { yyerror("The 'events' construct is currently not supported."); ;}
    break;

  case 43:
#line 188 "promela.y"
    { if((yyvsp[(1) - (1)].pSymTabNodeVal) != NULL) *globalSymTab = addToSymTab(*globalSymTab, (yyvsp[(1) - (1)].pSymTabNodeVal)); ;}
    break;

  case 44:
#line 189 "promela.y"
    { *globalSymTab = addToSymTab(*globalSymTab, (yyvsp[(1) - (1)].pSymTabNodeVal)); ;}
    break;

  case 45:
#line 190 "promela.y"
    { yyerror("Embedded C code is not supported."); ;}
    break;

  case 46:
#line 191 "promela.y"
    { yyerror("The 'named sequence' construct is currently not supported."); ;}
    break;

  case 48:
#line 201 "promela.y"
    {	(yyvsp[(9) - (9)].pFsmVal)->symTab = addToSymTab((yyvsp[(5) - (9)].pSymTabNodeVal), (yyvsp[(9) - (9)].pFsmVal)->symTab);
			  										*globalSymTab = addToSymTab(*globalSymTab, createSymTabNode(T_PROC, (yyvsp[(3) - (9)].sVal), nbrLines, 0, 0, (yyvsp[(1) - (9)].pExpVal), (yyvsp[(9) - (9)].pFsmVal), NULL));
												;}
    break;

  case 50:
#line 207 "promela.y"
    { yyerror("Deterministic proctypes are not supported (only useful for simulation)."); ;}
    break;

  case 51:
#line 210 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_CONST,  NULL, 0, NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 52:
#line 211 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_CONST,  NULL, 1, NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 53:
#line 212 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_CONST,  NULL, (yyvsp[(3) - (4)].iVal), NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 54:
#line 213 "promela.y"
    {	ptSymTabNode var = lookupInSymTab(*globalSymTab, (yyvsp[(3) - (4)].sVal));
													if(var == NULL) yyserror("The variable %s does not exist.", (yyvsp[(3) - (4)].sVal));
													else if(var->type != T_INT && var->type != T_BIT && var->type != T_SHORT) yyserror("The variable %s is not of type int, short or bit.", (yyvsp[(3) - (4)].sVal));
													else if(var->init == NULL || var->init->type != E_EXPR_CONST) yyserror("The variable %s does not have a constant value.", (yyvsp[(3) - (4)].sVal));
													else {
														(yyval.pExpVal) = createExpNode(E_EXPR_CONST,  NULL, var->init->iVal, NULL, NULL, NULL, nbrLines, NULL, NULL);
													}
													free((yyvsp[(3) - (4)].sVal));
												;}
    break;

  case 55:
#line 222 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_COUNT,  NULL, 	0, (yyvsp[(5) - (7)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); 
#if !defined(Z3) && !defined(MULTI)
                                         		  failure("Feature clones are not allowed in this variant of SNIP.\n");
#endif
												;}
    break;

  case 56:
#line 229 "promela.y"
    {	if(lookupInSymTab(*globalSymTab, "init") != NULL) yyerror("This is the second init process; only one is allowed.");
													else {
														*globalSymTab = addToSymTab(*globalSymTab, createSymTabNode(T_PROC, "init", nbrLines, 1, 0, NULL, (yyvsp[(3) - (3)].pFsmVal), NULL));
													}

												;}
    break;

  case 57:
#line 237 "promela.y"
    { 	if(neverClaim != NULL) yyerror("Found a second never claim.  Only one claim at a time is allowed.");
													else {
														neverClaim = createSymTabNode(T_NEVER, "__never", nbrLines, 1, 0, NULL, (yyvsp[(2) - (2)].pFsmVal), NULL);
														*globalSymTab = addToSymTab(*globalSymTab, neverClaim);
													}
												;}
    break;

  case 58:
#line 245 "promela.y"
    { yyerror("Event sequences (traces) are not supported."); ;}
    break;

  case 59:
#line 248 "promela.y"
    {	(yyval.pSymTabNodeVal) = createSymTabNode(T_TDEF, (yyvsp[(2) - (5)].sVal), nbrLines, 1, 0, NULL, NULL, (yyvsp[(4) - (5)].pSymTabNodeVal));
													if(!spinMode && strcmp((yyval.pSymTabNodeVal)->name, "features") == 0) {
														ptSymTabNode cur = (yyval.pSymTabNodeVal)->child;
														while(cur != NULL) {
															if(cur->type != T_BOOL && cur->type != T_UTYPE) failure("Feature '%s' is not declared as a boolean or a complex feature but as a '%s'.\n", cur->name, getTypeName(cur->type));
															if(cur->type == T_BOOL && cur->init && cur->init->type != E_EXPR_CONST) failure("The initial value of a Boolean feature must be given as a constant (0 or 1), this is not the case for '%s' at line %d.\n", cur->name, cur->lineNb);
															if(cur->type == T_BOOL)
																cur->type = T_FEAT;
#if defined Z3 || defined MULTI
															else {
																cur->type = T_UFEAT;
																if(!cur->utype || !cur->utype->child || cur->utype->child->type != T_BOOL || (strcmp(cur->utype->child->name, "is_in") != 0))
																	failure("The first subfield of a complex feature like '%s' must be a Boolean called 'is_in'. Here, it is a '%s' called '%s'. \n", cur->name, cur->utype->child ? getTypeName(cur->utype->child->type) : "NULL", cur->utype->child ? cur->utype->child->name : "NULL");
															}
#else 
															else
																failure("The use of complex features without Z3 is forbidden.\n");
															if(cur->bound > 1)
																failure("The use of feature clones without Z3 is forbidden.\n");
#endif
															if(!optimisedSpinMode) {
																cur->capacity = getFeatureID(cur->name);
																if(cur->capacity == -1) failure("Feature '%s' not found in feature model.\n", cur->name);
															}
															cur = cur->next;
														}
													}
												;}
    break;

  case 72:
#line 302 "promela.y"
    { (yyval.pFsmVal) = (yyvsp[(2) - (4)].pFsmVal); ;}
    break;

  case 73:
#line 305 "promela.y"
    { 	(yyval.pFsmVal) = createFsm();
													if((yyvsp[(1) - (1)].pExpVal)->type == E_DECL) {
														(yyval.pFsmVal)->symTab = (yyvsp[(1) - (1)].pExpVal)->symTab;
														(yyvsp[(1) - (1)].pExpVal)->symTab = NULL;
														destroyExpNode((yyvsp[(1) - (1)].pExpVal));
													} else if((yyvsp[(1) - (1)].pExpVal)->type == E_STMNT) {
														(yyval.pFsmVal) = stmnt2fsm((yyval.pFsmVal), (yyvsp[(1) - (1)].pExpVal)->children[0], *globalSymTab);
														(yyvsp[(1) - (1)].pExpVal)->children[0] = NULL;
														destroyExpNode((yyvsp[(1) - (1)].pExpVal));
													}
												;}
    break;

  case 74:
#line 316 "promela.y"
    {	if((yyvsp[(3) - (3)].pExpVal)->type == E_DECL) {
														(yyval.pFsmVal) = (yyvsp[(1) - (3)].pFsmVal);
														(yyval.pFsmVal)->symTab = addToSymTab((yyval.pFsmVal)->symTab, (yyvsp[(3) - (3)].pExpVal)->symTab);
														(yyvsp[(3) - (3)].pExpVal)->symTab = NULL;
														destroyExpNode((yyvsp[(3) - (3)].pExpVal));
													} else if((yyvsp[(3) - (3)].pExpVal)->type == E_STMNT) {
														(yyval.pFsmVal) = stmnt2fsm((yyvsp[(1) - (3)].pFsmVal), (yyvsp[(3) - (3)].pExpVal)->children[0], *globalSymTab);
														(yyvsp[(3) - (3)].pExpVal)->children[0] = NULL;
														destroyExpNode((yyvsp[(3) - (3)].pExpVal));
													}
												;}
    break;

  case 75:
#line 329 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_DECL, NULL, 0, NULL, NULL, NULL, nbrLines, NULL, (yyvsp[(1) - (1)].pSymTabNodeVal)); ;}
    break;

  case 76:
#line 330 "promela.y"
    { yyerror("Channel assertions are currently not supported."); ;}
    break;

  case 77:
#line 331 "promela.y"
    { yyerror("Declarations with labels are not suported."); ;}
    break;

  case 78:
#line 332 "promela.y"
    { yyerror("Channel assertions are currently not supported."); ;}
    break;

  case 79:
#line 333 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT, NULL, 0, (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 80:
#line 334 "promela.y"
    { yyerror("Unless statements are currently not supported."); ;}
    break;

  case 81:
#line 335 "promela.y"
    { 
		#ifndef CLOCK
													yyerror("Timed statements are allowed only together with the CLOCK option.");
		#endif
													(yyval.pExpVal) = createExpNode(E_STMNT, NULL, 0, (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL);; 
												;}
    break;

  case 82:
#line 343 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_WAIT, NULL, 0, (yyvsp[(3) - (5)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 83:
#line 344 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_WHEN, NULL, 0, (yyvsp[(3) - (6)].pExpVal), (yyvsp[(6) - (6)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 84:
#line 345 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_WHEN, NULL, 0, (yyvsp[(3) - (10)].pExpVal), (yyvsp[(10) - (10)].pExpVal), NULL, nbrLines, NULL, (yyvsp[(7) - (10)].pSymTabNodeVal)); ;}
    break;

  case 86:
#line 349 "promela.y"
    { yyerror("The 'hidden' keyword is not supported."); ;}
    break;

  case 87:
#line 350 "promela.y"
    { yyerror("The 'show' keyword is not supported."); ;}
    break;

  case 88:
#line 351 "promela.y"
    { yyerror("The 'local' keyword is not supported."); ;}
    break;

  case 91:
#line 361 "promela.y"
    {	ptSymTabNode cur = (yyvsp[(3) - (3)].pSymTabNodeVal);
													while(cur != NULL) {
														// If type != 0, then the var is a T_CHAN
														if(cur->type == 0) cur->type = (yyvsp[(2) - (3)].iVal);
														if(cur->type == T_CLOCK) { 
#ifdef CLOCK
															_clocks = listAdd(_clocks, cur->name);
#else
															yyerror("Clocks can only be declared when the CLOCK option is enabled.");
#endif
														}
														cur = cur->next;
													}
													(yyval.pSymTabNodeVal) = (yyvsp[(3) - (3)].pSymTabNodeVal);
												;}
    break;

  case 92:
#line 376 "promela.y"
    {	ptSymTabNode type = lookupInSymTab(*globalSymTab, (yyvsp[(2) - (3)].sVal));
	 												if(type == NULL) yyserror("The type %s was not declared in a typedef.", (yyvsp[(2) - (3)].sVal));
													else {
														ptSymTabNode cur = (yyvsp[(3) - (3)].pSymTabNodeVal);
														while(cur != NULL) {
															cur->type = T_UTYPE;
															cur->utype = type;
															cur = cur->next;
														}
														(yyval.pSymTabNodeVal) = (yyvsp[(3) - (3)].pSymTabNodeVal);
													}
	 												free((yyvsp[(2) - (3)].sVal));
												;}
    break;

  case 93:
#line 389 "promela.y"
    {	if((yyvsp[(2) - (6)].iVal) != T_MTYPE) yyerror("This syntax only works for MTYPEs.");
													(yyval.pSymTabNodeVal) = NULL;
													/* The mtype values are added in the nlst rule. */ 
												;}
    break;

  case 94:
#line 395 "promela.y"
    { (yyval.pSymTabNodeVal) = (yyvsp[(1) - (1)].pSymTabNodeVal); ;}
    break;

  case 95:
#line 396 "promela.y"
    { (yyval.pSymTabNodeVal) = addToSymTab((yyvsp[(1) - (3)].pSymTabNodeVal), (yyvsp[(3) - (3)].pSymTabNodeVal)); ;}
    break;

  case 96:
#line 400 "promela.y"
    { (yyval.pSymTabNodeVal) = NULL; ;}
    break;

  case 97:
#line 401 "promela.y"
    { (yyval.pSymTabNodeVal) = (yyvsp[(1) - (1)].pSymTabNodeVal); ;}
    break;

  case 100:
#line 408 "promela.y"
    { (yyval.pSymTabNodeVal) = (yyvsp[(1) - (1)].pSymTabNodeVal); ;}
    break;

  case 101:
#line 409 "promela.y"
    { (yyval.pSymTabNodeVal) = addToSymTab((yyvsp[(1) - (3)].pSymTabNodeVal), (yyvsp[(3) - (3)].pSymTabNodeVal)); ;}
    break;

  case 102:
#line 412 "promela.y"
    { (yyval.pSymTabNodeVal) = createSymTabNode(	 0, (yyvsp[(1) - (1)].pDataVal).sVal, nbrLines, (yyvsp[(1) - (1)].pDataVal).iVal,		  0, NULL, NULL, NULL); ;}
    break;

  case 103:
#line 413 "promela.y"
    { 	int mtype;
													if((yyvsp[(3) - (3)].pExpVal)->type == E_EXPR_VAR && (mtype = getMTypeValue(*mtypes, (yyvsp[(3) - (3)].pExpVal)->children[0]->children[0]->sVal)) != -1) {
														(yyvsp[(3) - (3)].pExpVal)->type = E_EXPR_CONST;
														(yyvsp[(3) - (3)].pExpVal)->iVal = mtype;
														destroyExpNode((yyvsp[(3) - (3)].pExpVal)->children[0]);
													}
													(yyval.pSymTabNodeVal) = createSymTabNode(	 0, (yyvsp[(1) - (3)].pDataVal).sVal, nbrLines, (yyvsp[(1) - (3)].pDataVal).iVal,	 	  0,   (yyvsp[(3) - (3)].pExpVal), NULL, NULL);
												;}
    break;

  case 104:
#line 421 "promela.y"
    { (yyval.pSymTabNodeVal) = createSymTabNode(T_CHAN, (yyvsp[(1) - (3)].pDataVal).sVal, nbrLines, (yyvsp[(1) - (3)].pDataVal).iVal, (yyvsp[(3) - (3)].pDataVal).iVal, NULL, NULL, (yyvsp[(3) - (3)].pDataVal).symTabNodeVal); ;}
    break;

  case 105:
#line 424 "promela.y"
    { (yyval.pDataVal).iVal = (yyvsp[(2) - (7)].iVal); (yyval.pDataVal).symTabNodeVal = (yyvsp[(6) - (7)].pSymTabNodeVal); ;}
    break;

  case 106:
#line 427 "promela.y"
    { (yyval.pDataVal).sVal = (yyvsp[(1) - (1)].sVal); (yyval.pDataVal).iVal = 1; ;}
    break;

  case 107:
#line 428 "promela.y"
    { yyerror("The 'unsigned' data type is not supported."); ;}
    break;

  case 108:
#line 429 "promela.y"
    { (yyval.pDataVal).sVal = (yyvsp[(1) - (4)].sVal); (yyval.pDataVal).iVal = (yyvsp[(3) - (4)].iVal); ;}
    break;

  case 109:
#line 432 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 110:
#line 435 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_VARREF_NAME, (yyvsp[(1) - (1)].sVal), 0, NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 111:
#line 436 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_VARREF_NAME,	(yyvsp[(1) - (4)].sVal), 0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 112:
#line 439 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_VARREF, 	  NULL, 0,   (yyvsp[(1) - (2)].pExpVal),   (yyvsp[(2) - (2)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 113:
#line 440 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_VARREF, 	  NULL, 1,   (yyvsp[(3) - (4)].pExpVal),   (yyvsp[(4) - (4)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 114:
#line 443 "promela.y"
    { (yyval.pExpVal) = NULL; ;}
    break;

  case 115:
#line 444 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(2) - (2)].pExpVal);   ;}
    break;

  case 116:
#line 447 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 117:
#line 448 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 118:
#line 451 "promela.y"
    { 
													(yyval.pExpVal) = createExpNode(E_STMNT_CHAN_RCV, 	NULL, 	0,   (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); 
#ifdef Z3
													ptExpNode varNode = (yyval.pExpVal)->children[1];
													while(varNode) {
														if(varNode->children[0] && varNode->children[0]->type == E_RARG_VAR && varNode->children[0]->children[0] && varNode->children[0]->children[0]->children[0]) {
															ptSymTabNode featureNode = lookupInSymTab(*globalSymTab, varNode->children[0]->children[0]->children[0]->sVal);
															if(featureNode && featureNode->utype && featureNode->utype->child) {
																ptSymTabNode node = lookupInSymTab(featureNode->utype->child, varNode->children[0]->children[0]->children[1]->children[0]->sVal);
																if(node && node->type == T_FEAT || node->type == T_UFEAT)
																	failure("Features cannot occur in right-hand side of channel reception statements at line %d.\n", nbrLines);
															}
														}
														varNode = varNode->children[1];
													}
#endif
												;}
    break;

  case 119:
#line 468 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_CHAN_SND, 	NULL, 	0,   (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 120:
#line 469 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_IF, 		NULL, 	0,   (yyvsp[(2) - (3)].pExpVal), NULL, NULL,       (yyvsp[(1) - (3)].iVal), NULL, NULL); ;}
    break;

  case 121:
#line 470 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_DO, 		NULL, 	0,   (yyvsp[(2) - (3)].pExpVal), NULL, NULL,       (yyvsp[(1) - (3)].iVal), NULL, NULL); ;}
    break;

  case 122:
#line 471 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_BREAK, 	NULL, 	0, NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 123:
#line 472 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_GOTO, 		(yyvsp[(2) - (2)].sVal), 	0, NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 124:
#line 473 "promela.y"
    { if((yyvsp[(3) - (3)].pExpVal)->type == E_STMNT_LABEL && (yyvsp[(3) - (3)].pExpVal)->children[0] && (yyvsp[(3) - (3)].pExpVal)->children[0]->type == E_STMNT_LABEL) yyerror("Only two labels per state are supported."); 
												  (yyval.pExpVal) = createExpNode(E_STMNT_LABEL,		(yyvsp[(1) - (3)].sVal), 	0,   (yyvsp[(3) - (3)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 125:
#line 477 "promela.y"
    { 
													(yyval.pExpVal) = createExpNode(E_STMNT_ASGN, 		NULL, 	0,   (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL);
#ifdef Z3													
													if((yyvsp[(1) - (3)].pExpVal) && (yyvsp[(1) - (3)].pExpVal)->children[1] && (yyvsp[(1) - (3)].pExpVal)->children[1]->children[0]) {
														ptSymTabNode featureNode = lookupInSymTab(*globalSymTab, (yyvsp[(1) - (3)].pExpVal)->children[0]->sVal);
														if(featureNode && featureNode->utype && featureNode->utype->child) {
															ptSymTabNode node = lookupInSymTab(featureNode->utype->child, (yyvsp[(1) - (3)].pExpVal)->children[1]->children[0]->sVal);
															if(node && node->type == T_FEAT || node->type == T_UFEAT)
																failure("Features cannot occur in left-hand side of assignment at line %d.\n", nbrLines);
														}
													}
#endif
												;}
    break;

  case 126:
#line 490 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_INCR, 		NULL, 	0,   (yyvsp[(1) - (2)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 127:
#line 491 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_DECR, 		NULL, 	0,   (yyvsp[(1) - (2)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 128:
#line 492 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_PRINT, 	(yyvsp[(3) - (5)].sVal), 	0,   (yyvsp[(4) - (5)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 129:
#line 493 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_PRINTM, 	NULL, 	0,   (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 130:
#line 494 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_PRINTM, 	NULL,  (yyvsp[(3) - (4)].iVal), NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 131:
#line 495 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_ASSERT, 	NULL, 	0,   (yyvsp[(2) - (2)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 132:
#line 496 "promela.y"
    { yyerror("Embedded C code is not supported."); ;}
    break;

  case 133:
#line 497 "promela.y"
    { yyerror("Sorted send and random receive are not supported."); ;}
    break;

  case 134:
#line 498 "promela.y"
    { yyerror("Channel poll operations are not supported."); ;}
    break;

  case 135:
#line 499 "promela.y"
    { yyerror("Channel poll operations are not supported."); ;}
    break;

  case 136:
#line 500 "promela.y"
    { yyerror("Sorted send and random receive are not supported."); ;}
    break;

  case 137:
#line 501 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_EXPR, 		NULL, 	0,   (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 138:
#line 502 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_ELSE, 		NULL, 	0, NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 139:
#line 503 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_ATOMIC,	NULL, 	0, NULL, NULL, NULL, nbrLines,   (yyvsp[(3) - (5)].pFsmVal), NULL); ;}
    break;

  case 140:
#line 504 "promela.y"
    { yyerror("Deterministic steps are not yet supported."); ;}
    break;

  case 141:
#line 505 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_SEQ, 		NULL, 	0, NULL, NULL, NULL, nbrLines,   (yyvsp[(2) - (4)].pFsmVal), NULL); ;}
    break;

  case 142:
#line 506 "promela.y"
    { yyerror("Inline calls are not yet supported."); ;}
    break;

  case 143:
#line 509 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_OPT, 		NULL, 	0, NULL, NULL, NULL, nbrLines,   (yyvsp[(1) - (1)].pFsmVal), NULL); ;}
    break;

  case 144:
#line 510 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_STMNT_OPT, 		NULL, 	0,   (yyvsp[(2) - (2)].pExpVal), NULL, NULL, nbrLines,   (yyvsp[(1) - (2)].pFsmVal), NULL); ;}
    break;

  case 145:
#line 513 "promela.y"
    { (yyval.pFsmVal) = (yyvsp[(2) - (3)].pFsmVal); ;}
    break;

  case 147:
#line 517 "promela.y"
    { /* redundant semi at end of sequence */ ;}
    break;

  case 148:
#line 520 "promela.y"
    { /* at least one semi-colon */ ;}
    break;

  case 149:
#line 521 "promela.y"
    { /* but more are okay too   */ ;}
    break;

  case 150:
#line 524 "promela.y"
    { (yyval.sVal) = (yyvsp[(1) - (1)].sVal); ;}
    break;

  case 151:
#line 525 "promela.y"
    { (yyval.sVal) = (yyvsp[(1) - (1)].sVal); ;}
    break;

  case 152:
#line 528 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_PAR, 	NULL, 	0, (yyvsp[(2) - (3)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 153:
#line 529 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_PLUS, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 154:
#line 530 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_MINUS, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 155:
#line 531 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_TIMES, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 156:
#line 532 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_DIV, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 157:
#line 533 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_MOD, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 158:
#line 534 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_BITWAND,NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 159:
#line 535 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_BITWXOR,NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 160:
#line 536 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_BITWOR,	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 161:
#line 537 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_GT, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 162:
#line 538 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_LT, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 163:
#line 539 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_GE, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 164:
#line 540 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_LE, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 165:
#line 541 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_EQ, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 166:
#line 542 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_NE, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 167:
#line 543 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_AND, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 168:
#line 544 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_OR, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 169:
#line 545 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_LSHIFT,	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 170:
#line 546 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_RSHIFT,	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 171:
#line 547 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_COUNT,  NULL, 	0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 172:
#line 548 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_BITWNEG,NULL, 	0, (yyvsp[(2) - (2)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 173:
#line 549 "promela.y"
    { 	if((yyvsp[(2) - (2)].pExpVal)->type != E_EXPR_CONST) (yyval.pExpVal) = createExpNode(E_EXPR_UMIN,	NULL, 	0, (yyvsp[(2) - (2)].pExpVal), NULL, NULL, nbrLines, NULL, NULL);
													else {
														(yyvsp[(2) - (2)].pExpVal)->iVal = - (yyvsp[(2) - (2)].pExpVal)->iVal;
														(yyval.pExpVal) = (yyvsp[(2) - (2)].pExpVal);
													}
												;}
    break;

  case 174:
#line 555 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_NEG, 	NULL, 	0, (yyvsp[(2) - (2)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 175:
#line 556 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_COND, 	NULL, 	0, (yyvsp[(2) - (7)].pExpVal),   (yyvsp[(4) - (7)].pExpVal),   (yyvsp[(6) - (7)].pExpVal), nbrLines, NULL, NULL); ;}
    break;

  case 176:
#line 557 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_RUN, 	  (yyvsp[(2) - (6)].sVal),   0, (yyvsp[(4) - (6)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 177:
#line 559 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_RUN, 	  (yyvsp[(2) - (9)].sVal),   0, (yyvsp[(7) - (9)].pExpVal), (yyvsp[(4) - (9)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 178:
#line 560 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_LEN, 	NULL, 	0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 179:
#line 561 "promela.y"
    { yyerror("The enabled keyword is not supported."); ;}
    break;

  case 180:
#line 562 "promela.y"
    { yyerror("Construct not supported."); /* Unclear */ ;}
    break;

  case 181:
#line 563 "promela.y"
    { yyerror("Sorted send and random receive are not supported."); ;}
    break;

  case 182:
#line 564 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_VAR, 	NULL, 	0, (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 183:
#line 565 "promela.y"
    { yyerror("Embedded C code is not supported."); ;}
    break;

  case 184:
#line 566 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_CONST,  NULL,(yyvsp[(1) - (1)].iVal), NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 185:
#line 567 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_TIMEOUT,NULL, 0, NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 186:
#line 568 "promela.y"
    { yyerror("The 'np_' variable is not supported."); ;}
    break;

  case 187:
#line 569 "promela.y"
    { yyerror("The 'pc_value()' construct is not supported."); ;}
    break;

  case 188:
#line 570 "promela.y"
    { yyerror("Construct not supported."); /* Unclear */ ;}
    break;

  case 189:
#line 571 "promela.y"
    { yyerror("Construct not supported."); /* Unclear */ ;}
    break;

  case 190:
#line 572 "promela.y"
    { yyerror("Construct not supported."); /* Unclear */ ;}
    break;

  case 191:
#line 573 "promela.y"
    { yyerror("Construct not supported."); /* Unclear */ ;}
    break;

  case 193:
#line 577 "promela.y"
    { yyerror("The 'priority' construct is related to simulation and not supported."); ;}
    break;

  case 194:
#line 580 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 195:
#line 581 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 197:
#line 585 "promela.y"
    { yyerror("The 'provided' construct is currently not supported."); ;}
    break;

  case 198:
#line 590 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 199:
#line 591 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_PAR, 	NULL, 	0, (yyvsp[(2) - (3)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 200:
#line 592 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_AND, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 201:
#line 593 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_AND, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 202:
#line 594 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_OR, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 203:
#line 595 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_OR, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 204:
#line 596 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_AND, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 205:
#line 597 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_OR, 	NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 206:
#line 599 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_FULL, 	NULL, 	0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 207:
#line 600 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_NFULL, 	NULL, 	0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 208:
#line 601 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_EMPTY, 	NULL, 	0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 209:
#line 602 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_EXPR_NEMPTY,	NULL, 	0, (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 210:
#line 605 "promela.y"
    { (yyval.pDataVal).sVal = NULL; (yyval.pDataVal).iVal = (yyvsp[(1) - (1)].iVal); ;}
    break;

  case 211:
#line 606 "promela.y"
    { (yyval.pDataVal).sVal = (yyvsp[(1) - (1)].sVal);   (yyval.pDataVal).iVal = -1; ;}
    break;

  case 212:
#line 610 "promela.y"
    {	if((yyvsp[(1) - (1)].pDataVal).iVal > -1) {
														(yyval.pSymTabNodeVal) = createSymTabNode((yyvsp[(1) - (1)].pDataVal).iVal, NULL, nbrLines, 1, 0, NULL, NULL, NULL);
													} else {
														(yyval.pSymTabNodeVal) = createSymTabNodeUType(*globalSymTab, (yyvsp[(1) - (1)].pDataVal).sVal, NULL, nbrLines, 1, 0, NULL, NULL, NULL);
														if((yyval.pSymTabNodeVal) == NULL) yyerror("The type %s was not declared in a typedef.", (yyvsp[(1) - (1)].pDataVal).sVal);
														free((yyvsp[(1) - (1)].pDataVal).sVal);
													}
												;}
    break;

  case 213:
#line 618 "promela.y"
    {	if((yyvsp[(1) - (3)].pDataVal).iVal > -1) {
														(yyval.pSymTabNodeVal) = addToSymTab(createSymTabNode((yyvsp[(1) - (3)].pDataVal).iVal, NULL, nbrLines, 1, 0, NULL, NULL, NULL), (yyvsp[(3) - (3)].pSymTabNodeVal));
													} else {
														ptSymTabNode temp = createSymTabNodeUType(*globalSymTab, (yyvsp[(1) - (3)].pDataVal).sVal, NULL, nbrLines, 1, 0, NULL, NULL, NULL);
														if(temp == NULL) yyerror("The type %s was not declared in a typedef.", (yyvsp[(1) - (3)].pDataVal).sVal);
														else (yyval.pSymTabNodeVal) = addToSymTab(temp, (yyvsp[(3) - (3)].pSymTabNodeVal));
														free((yyvsp[(1) - (3)].pDataVal).sVal);
													}
												;}
    break;

  case 214:
#line 630 "promela.y"
    { (yyval.pExpVal) = NULL; ;}
    break;

  case 215:
#line 631 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 216:
#line 635 "promela.y"
    { (yyval.pExpVal) = NULL; ;}
    break;

  case 217:
#line 636 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(2) - (2)].pExpVal); ;}
    break;

  case 218:
#line 640 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(1) - (1)].pExpVal); ;}
    break;

  case 219:
#line 641 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_ARGLIST,		NULL, 	0, (yyvsp[(1) - (4)].pExpVal),   (yyvsp[(3) - (4)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 220:
#line 644 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_ARGLIST,		NULL, 	0, (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 221:
#line 645 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_ARGLIST,		NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 222:
#line 648 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_RARG_VAR,	NULL, 	0,   (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 223:
#line 649 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_RARG_EVAL,	NULL, 	0,   (yyvsp[(3) - (4)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 224:
#line 650 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_RARG_CONST,	NULL,  (yyvsp[(1) - (1)].iVal), NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 225:
#line 651 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_RARG_CONST,	NULL, -(yyvsp[(2) - (2)].iVal), NULL, NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 226:
#line 655 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_ARGLIST,		NULL, 	0, (yyvsp[(1) - (1)].pExpVal), NULL, NULL, nbrLines, NULL, NULL); ;}
    break;

  case 227:
#line 656 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_ARGLIST,		NULL, 	0, (yyvsp[(1) - (3)].pExpVal),   (yyvsp[(3) - (3)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 228:
#line 657 "promela.y"
    { (yyval.pExpVal) = createExpNode(E_ARGLIST,		NULL, 	0, (yyvsp[(1) - (4)].pExpVal),   (yyvsp[(3) - (4)].pExpVal), NULL, nbrLines, NULL, NULL); ;}
    break;

  case 229:
#line 658 "promela.y"
    { (yyval.pExpVal) = (yyvsp[(2) - (3)].pExpVal); ;}
    break;

  case 230:
#line 661 "promela.y"
    { *mtypes = addMType(*mtypes, (yyvsp[(1) - (1)].sVal)); ;}
    break;

  case 231:
#line 662 "promela.y"
    { *mtypes = addMType(*mtypes, (yyvsp[(2) - (2)].sVal)); ;}
    break;


/* Line 1267 of yacc.c.  */
#line 3443 "promela.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (globalSymTab, mtypes, property, YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (globalSymTab, mtypes, property, yymsg);
	  }
	else
	  {
	    yyerror (globalSymTab, mtypes, property, YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, globalSymTab, mtypes, property);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, globalSymTab, mtypes, property);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (globalSymTab, mtypes, property, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, globalSymTab, mtypes, property);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, globalSymTab, mtypes, property);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 665 "promela.y"



int yyerror(char *msg) {
	fprintf(stderr, "Syntax error on line %d: '%s'.\n", nbrLines, msg);
	exit(1);
}

int yyserror(char *msg, char *param) {
	char buffer [strlen(msg) + strlen(param) + 1];
	sprintf(buffer, msg, param);
	fprintf(stderr, "Syntax error on line %d: '%s'.\n", nbrLines, buffer);
	exit(1);
}

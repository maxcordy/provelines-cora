
// Generic bug display function
void failure(char* msg, ...); // Prints an error message and quit the execution.
void warning(char* msg, ...); // Prints an error message without quitting the execution.

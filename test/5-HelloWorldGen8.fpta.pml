check <> (t0_end && t1_end && d2_end && t3_end && t4_end && t5_end && t6_end && d7_end && t8_end && t9_end && t10_end && t11_end && d12_end && t13_end && t14_end && d15_end && plt_intram0_idle && plt_extrom0_idle && plt_p0_c0_idle && plt_p0_c1_idle) within 192000
typedef features{
bool DesignSpaceVariability;
bool PlatformVariability;
bool Plt_P0;
bool DeploymentVariability;
bool D2_On;
bool D2_On_Plt_ExtROM0;
bool D2_On_Plt_IntRAM0;
bool D12_On;
bool D12_On_Plt_ExtROM0;
bool D12_On_Plt_IntRAM0;
bool D15_On;
bool D15_On_Plt_ExtROM0;
bool D15_On_Plt_IntRAM0;
bool D7_On;
bool D7_On_Plt_ExtROM0;
bool D7_On_Plt_IntRAM0;
bool PT14_T13_On;
bool PT14_T13_On_Plt_IntRAM0;
bool PD12_T11_On;
bool PD12_T11_On_Plt_ExtROM0;
bool PD12_T11_On_Plt_IntRAM0;
bool PD15_T14_On;
bool PD15_T14_On_Plt_ExtROM0;
bool PD15_T14_On_Plt_IntRAM0;
bool PT8_T9_On;
bool PT8_T9_On_Plt_IntRAM0;
bool PD7_T8_On;
bool PD7_T8_On_Plt_ExtROM0;
bool PD7_T8_On_Plt_IntRAM0;
bool PT6_T5_On;
bool PT6_T5_On_Plt_P0_C1_R6_0;
bool PT6_T5_On_Plt_IntRAM0;
bool PD7_T6_On;
bool PD7_T6_On_Plt_ExtROM0;
bool PD7_T6_On_Plt_IntRAM0;
bool PT1_T0_On;
bool PT1_T0_On_Plt_P0_C0_R6_1;
bool PT1_T0_On_Plt_IntRAM0;
bool PD2_T3_On;
bool PD2_T3_On_Plt_ExtROM0;
bool PD2_T3_On_Plt_IntRAM0;
bool PD2_T1_On;
bool PD2_T1_On_Plt_ExtROM0;
bool PD2_T1_On_Plt_IntRAM0;
bool PT1_T5_On;
bool PT1_T5_On_Plt_IntRAM0;
bool PT11_T10_On;
bool PT11_T10_On_Plt_IntRAM0;
bool PD12_T13_On;
bool PD12_T13_On_Plt_ExtROM0;
bool PD12_T13_On_Plt_IntRAM0;
bool PT10_T9_On;
bool PT10_T9_On_Plt_IntRAM0;
bool PT3_T4_On;
bool PT3_T4_On_Plt_P0_C0_R3_0;
bool PT3_T4_On_Plt_IntRAM0;
bool T10_On;
bool T10_On_Plt_P0_C1_P2;
bool T3_On;
bool T3_On_Plt_P0_C0_P3;
bool T3_On_Plt_P0_C1_P6;
bool T14_On;
bool T14_On_Plt_P0_C0_P3;
bool T14_On_Plt_P0_C1_P6;
bool T6_On;
bool T6_On_Plt_P0_C0_P3;
bool T6_On_Plt_P0_C1_P6;
bool T11_On;
bool T11_On_Plt_P0_C0_P3;
bool T11_On_Plt_P0_C1_P6;
bool T8_On;
bool T8_On_Plt_P0_C0_P5;
bool T4_On;
bool T4_On_Plt_P0_C0_P0;
bool T13_On;
bool T13_On_Plt_P0_C0_P7;
bool T9_On;
bool T9_On_Plt_P0_C1_P5;
bool T5_On;
bool T5_On_Plt_P0_C1_P5;
bool T1_On;
bool T1_On_Plt_P0_C0_P6;
bool T0_On;
bool T0_On_Plt_P0_C0_P0
}features f;
//design number = 73728.0
#define main_freq 200
#define burst 512

#define NB_MEMORY 20
#define NULL_MEMORY -1
#define Plt_P0_C0_R1_0_ID 0
#define Plt_P0_C0_R8_0_ID 1
#define Plt_P0_C0_R4_0_ID 2
#define Plt_P0_C0_R3_0_ID 3
#define Plt_P0_C0_R5_0_ID 4
#define Plt_P0_C0_R6_0_ID 5
#define Plt_P0_C0_R6_1_ID 6
#define Plt_P0_C0_R2_0_ID 7
#define Plt_P0_C0_R9_0_ID 8
#define Plt_P0_C1_R0_0_ID 9
#define Plt_P0_C1_R1_0_ID 10
#define Plt_P0_C1_R4_0_ID 11
#define Plt_P0_C1_R4_1_ID 12
#define Plt_P0_C1_R2_0_ID 13
#define Plt_P0_C1_R6_0_ID 14
#define Plt_P0_C1_R7_0_ID 15
#define Plt_P0_C1_R9_0_ID 16
#define Plt_P0_C1_R8_0_ID 17
#define Plt_IntRAM0_ID 18
#define Plt_ExtROM0_ID 19


#define NB_PROCESSOR 20
#define NULL_PROCESSOR -1
#define Plt_P0_C0_P0_ID 0
#define Plt_P0_C0_P1_ID 1
#define Plt_P0_C0_P2_ID 2
#define Plt_P0_C0_P3_ID 3
#define Plt_P0_C0_P4_ID 4
#define Plt_P0_C0_P5_ID 5
#define Plt_P0_C0_P6_ID 6
#define Plt_P0_C0_P7_ID 7
#define Plt_P0_C0_P8_ID 8
#define Plt_P0_C0_P9_ID 9
#define Plt_P0_C1_P0_ID 10
#define Plt_P0_C1_P1_ID 11
#define Plt_P0_C1_P2_ID 12
#define Plt_P0_C1_P3_ID 13
#define Plt_P0_C1_P4_ID 14
#define Plt_P0_C1_P5_ID 15
#define Plt_P0_C1_P6_ID 16
#define Plt_P0_C1_P7_ID 17
#define Plt_P0_C1_P8_ID 18
#define Plt_P0_C1_P9_ID 19

#define NB_PU 2
#define NULL_PU -1
#define Plt_P0_C0_ID 0
#define Plt_P0_C1_ID 1
typedef data{
	byte id;
	byte loc;
	int size
}

#define MAX_NB_INS 2
#define MAX_NB_OUTS 2

typedef command{
	byte fct;
	data ins[MAX_NB_INS];
	byte nb_ins;
	data outs[MAX_NB_OUTS];
	byte nb_outs
}

#define NB_TASK 12

#define LIST_LENGTH NB_TASK

typedef dependency{
	byte tasks[NB_TASK];
	byte depth
}

typedef command_list{
	command cmd[LIST_LENGTH];
	dependency dep[LIST_LENGTH];
	int size
}

chan transfer[NB_MEMORY] = [0] of {data};
chan send[NB_PU] = [0] of {command};
chan recv[NB_PU] = [0] of {command};



bool plt_intram0_idle = true; 

active proctype storage_Plt_IntRAM0(){ atomic {
	int freq = 100;
	int bpc = 2;
	int latency =2;
	int _delay = 0;
	data in;
	freq = 100;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_IntRAM0_ID]?in -> plt_intram0_idle = false; 
		wait(_delay) then skip;
		plt_intram0_idle = true; 
	od;
};
}

bool plt_extrom0_idle = true; 

active proctype storage_Plt_ExtROM0(){ atomic {
	int freq = 50;
	int bpc = 2;
	int latency =4;
	int _delay = 0;
	data in;
	freq = 50;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_ExtROM0_ID]?in -> plt_extrom0_idle = false; 
		wait(_delay) then skip;
		plt_extrom0_idle = true; 
	od;
};
}
#define data_copy(dst, src) dst.id = src.id; dst.loc = src.loc; dst.size = src.size;

#define cmd_copy(dst, src) dst.fct = src.fct; dst.nb_ins = src.nb_ins; dst.nb_outs = src.nb_outs; \
	j = 0;																\
	do																	\
	:: j < src.nb_ins -> data_copy(dst.ins[j], src.ins[j]) j++			\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < src.nb_outs -> data_copy(dst.outs[j], src.outs[j]) j++		\
	:: else -> break;													\
	od;

#define free_data(data) data.id = 0; data.loc = 0; data.size = 0;

#define free_cmd(cmd)  \
	j = 0;																\
	do																	\
	:: j < cmd.nb_ins -> free_data(cmd.ins[j]) j++						\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < cmd.nb_outs -> free_data(cmd.outs[j]) j++					\
	:: else -> break;													\
	od;										\
	cmd.fct = 0; cmd.nb_ins = 0; cmd.nb_outs = 0;
	
#define is_external(id)	_is_external = id == Plt_IntRAM0_ID || id == Plt_ExtROM0_ID;
#define inactive(cmd) _inactive = cmd.fct == 0 && cmd.nb_ins == 0 && cmd.nb_outs == 0;

#define declare_var()													\
	int i_cmd; int _delay;												\
	int i_time_cmd;														\
	int	i, j, k, l, m;													\
	bool _is_flush_cmd, _time_cmd, _hasCompleted, _is_external, _inactive;\
	command_list list;													\

#define allocate() 													\
	i = 0;																\
	do																	\
	::i < list.size ->													\
		inactive(list.cmd[i])											\
		if																\
		::_inactive -> i_cmd = i; break;								\
		::else -> skip;													\
		fi;																\
		i++;															\
	::else -> i_cmd = list.size; list.size++; break;					\
	od;																	\
	cmd_copy(list.cmd[i_cmd], cmd)										\
	list.dep[i_cmd].tasks[0] = i_cmd; 									\
	list.dep[i_cmd].depth = 1;

#define cmd_list_push()												\
	allocate()															\
	j = 0;																\
	do																	\
	::j < cmd.nb_ins ->													\
		k = 0;															\
		do																\
		:: k < list.size ->												\
			l = 0;														\
			do															\
			:: l < list.cmd[k].nb_outs ->								\
				if 														\
				:: list.cmd[k].outs[l].id == cmd.ins[j].id -> m = 0;	\
					do													\
					:: m < list.dep[k].depth ->							\
						list.dep[i_cmd].tasks[list.dep[i_cmd].depth] = list.dep[k].tasks[m];\
						list.dep[i_cmd].depth++;						\
						m++;											\
					:: else -> break;									\
					od;													\
				:: else -> break;										\
				fi;														\
				l++;													\
			:: else -> break;											\
			od;															\
			k++;														\
		:: else -> break;												\
		od;																\
		j++;															\
	:: else -> break;													\
	od;

#define free_cmd_list()												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		free_cmd(list.cmd[k])											\
		i++;															\
	:: else -> list.dep[i_cmd].depth = 0; break;						\
	od;

#define is_flush_cmd()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		is_external(cmd.outs[i].loc)									\
		if 																\
		:: !_is_external -> _is_flush_cmd = false; break;				\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> _is_flush_cmd = true; break;								\
	od;


#define prog_pipeline()												\
	nb_ins = 0; nb_outs = 0;											\
	i_ins = 0; i_outs = 0;												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_ins ->									\
			is_external(list.cmd[k].ins[j].loc)							\
			if															\
			:: _is_external -> 											\
				fetch[nb_ins] = 0;										\
				data_copy(ins[nb_ins], list.cmd[k].ins[j]) nb_ins++;	\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_outs ->									\
			is_external(list.cmd[k].outs[j].loc)						\
			if															\
			:: _is_external -> 											\
				store[nb_outs] = 0;										\
				data_copy(outs[nb_outs], list.cmd[k].outs[j]) nb_outs++;\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		i++;															\
	:: else -> break;													\
	od;

#define hasCompleted()													\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		:: fetch[i] < ins[i].size -> _hasCompleted = false; break;		\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> _hasCompleted = true; break;								\
	od;																	\
	i = 0;																\
	do																	\
	:: i < nb_outs && _hasCompleted ->									\
		if																\
		:: store[i] < outs[i].size -> _hasCompleted = false; break; 	\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> break;													\
	od;

#define checkIns()														\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		::fetch[i] >= ins[i].size -> nb_ins--;							\
			data_copy(ins[i], ins[nb_ins])								\
			fetch[i] = fetch[nb_ins];									\
			fetch[nb_ins] = 0;											\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> i_ins = 0; break;										\
	od;

#define checkOuts()														\
	i = 0;																\
	do																	\
	:: i < nb_outs ->													\
		if																\
		::store[i] >= outs[i].size -> nb_outs--;						\
			data_copy(outs[i], outs[nb_outs])							\
			store[i] = store[nb_outs];									\
			store[nb_outs] = 0;											\
		::else -> skip;													\
		fi;																\
		i++;															\
	:: else -> i_outs = 0; break;										\
	od;

#define setOutputs()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		cmd.outs[i].size = cmd.ins[0].size;	i++;						\
	:: else -> break;													\
	od;

#define time_cmd(cmd)												\
	if																	\
	::cmd.fct == Plt_P0_C0_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P7_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P8_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P9_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P7_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P8_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P9_ID -> _time_cmd = 4;					\
	fi;
#define time_cmds()														\
	time_cmd(list.cmd[ list.dep[i_cmd].tasks[0] ])						\
	l = _time_cmd;														\
	i = 1;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		l = _time_cmd;													\
		time_cmd(list.cmd[k])											\
		i++;															\
		if																\
		::_time_cmd < l -> l = _time_cmd;								\
		:: else -> skip;												\
		fi;																\
	:: else -> break;													\
	od;																	\
	_delay = (burst/l)*(main_freq/freq);

bool plt_p0_c0_idle = true;

active proctype processor_Plt_P0_C0(){ atomic {
	int freq = 200;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 200;
do
	::send[Plt_P0_C0_ID]?cmd -> plt_p0_c0_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P0_C0_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P0_C0_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p0_c0_idle = true;
	od;
};
}

bool plt_p0_c1_idle = true;

active proctype processor_Plt_P0_C1(){ atomic {
	int freq = 200;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 200;
do
	::send[Plt_P0_C1_ID]?cmd -> plt_p0_c1_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P0_C1_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P0_C1_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p0_c1_idle = true;
	od;
};
}

chan PT1_T5 = [0] of {data}
chan PT1_T0 = [0] of {data}
chan PD2_T3 = [0] of {data}
chan PD2_T1 = [0] of {data}
chan PT3_T4 = [0] of {data}
chan PT6_T5 = [0] of {data}
chan PD7_T8 = [0] of {data}
chan PD7_T6 = [0] of {data}
chan PT8_T9 = [0] of {data}
chan PT10_T9 = [0] of {data}
chan PT11_T10 = [0] of {data}
chan PD12_T13 = [0] of {data}
chan PD12_T11 = [0] of {data}
chan PT14_T13 = [0] of {data}
chan PD15_T14 = [0] of {data}

bool d15_end = false;

active proctype input_D15(){ atomic {
	data in;
	in.id = 1;
	in.size = 512;
	gd
	::f.D15_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D15_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD15_T14!in;
	d15_end = true;
};
}


bool t14_end = false;

active proctype T14(){ atomic {
	command cmd;

	byte proc = 0;
	byte pu = 0;
	data PD15_T14_d;
	data PT14_T13_d;
	PD15_T14?PD15_T14_d;
	gd
	::f.T14_On_Plt_P0_C0_P3 -> proc = Plt_P0_C0_P3_ID; pu = Plt_P0_C0_ID;
	::f.T14_On_Plt_P0_C1_P6 -> proc = Plt_P0_C1_P6_ID; pu = Plt_P0_C1_ID;
	dg;
	PT14_T13_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD15_T14_d)
	cmd.nb_ins = 1;
	PT14_T13_d.id = 2;PT14_T13_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT14_T13_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT14_T13_d, cmd.outs[0])
	PT14_T13!PT14_T13_d;
	t14_end = true;
};
}


bool d12_end = false;

active proctype input_D12(){ atomic {
	data in;
	in.id = 3;
	in.size = 256;
	gd
	::f.D12_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D12_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD12_T13!in;

	PD12_T11!in;
	d12_end = true;
};
}


bool t13_end = false;

active proctype T13(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C0_P7_ID;
	byte pu = Plt_P0_C0_ID;
	data PD12_T13_d;
	data PT14_T13_d;
	PD12_T13?PD12_T13_d;
	PT14_T13?PT14_T13_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD12_T13_d)
	data_copy(cmd.ins[1], PT14_T13_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t13_end = true;
};
}


bool t11_end = false;

active proctype T11(){ atomic {
	command cmd;

	byte proc = 0;
	byte pu = 0;
	data PD12_T11_d;
	data PT11_T10_d;
	PD12_T11?PD12_T11_d;
	gd
	::f.T11_On_Plt_P0_C0_P3 -> proc = Plt_P0_C0_P3_ID; pu = Plt_P0_C0_ID;
	::f.T11_On_Plt_P0_C1_P6 -> proc = Plt_P0_C1_P6_ID; pu = Plt_P0_C1_ID;
	dg;
	PT11_T10_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD12_T11_d)
	cmd.nb_ins = 1;
	PT11_T10_d.id = 4;PT11_T10_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT11_T10_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT11_T10_d, cmd.outs[0])
	PT11_T10!PT11_T10_d;
	t11_end = true;
};
}


bool t10_end = false;

active proctype T10(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C1_P2_ID;
	byte pu = Plt_P0_C1_ID;
	data PT11_T10_d;
	data PT10_T9_d;
	PT11_T10?PT11_T10_d;
	PT10_T9_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT11_T10_d)
	cmd.nb_ins = 1;
	PT10_T9_d.id = 5;PT10_T9_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT10_T9_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT10_T9_d, cmd.outs[0])
	PT10_T9!PT10_T9_d;
	t10_end = true;
};
}


bool d7_end = false;

active proctype input_D7(){ atomic {
	qualityLoss + 1;
	data in;
	in.id = 6;
	in.size = 256;
	gd
	::f.D7_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D7_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD7_T6!in;

	PD7_T8!in;
	d7_end = true;
};
}


bool t8_end = false;

active proctype T8(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P5_ID;
	byte pu = Plt_P0_C0_ID;
	data PD7_T8_d;
	data PT8_T9_d;
	PD7_T8?PD7_T8_d;
	PT8_T9_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD7_T8_d)
	cmd.nb_ins = 1;
	PT8_T9_d.id = 7;PT8_T9_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT8_T9_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT8_T9_d, cmd.outs[0])
	PT8_T9!PT8_T9_d;
	t8_end = true;
};
}


bool t9_end = false;

active proctype T9(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P0_C1_P5_ID;
	byte pu = Plt_P0_C1_ID;
	data PT8_T9_d;
	data PT10_T9_d;
	PT8_T9?PT8_T9_d;
	PT10_T9?PT10_T9_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT8_T9_d)
	data_copy(cmd.ins[1], PT10_T9_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t9_end = true;
};
}


bool t6_end = false;

active proctype T6(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = 0;
	byte pu = 0;
	data PD7_T6_d;
	data PT6_T5_d;
	PD7_T6?PD7_T6_d;
	gd
	::f.T6_On_Plt_P0_C0_P3 -> proc = Plt_P0_C0_P3_ID; pu = Plt_P0_C0_ID;
	::f.T6_On_Plt_P0_C1_P6 -> proc = Plt_P0_C1_P6_ID; pu = Plt_P0_C1_ID;
	dg;
	gd
	::f.PT6_T5_On_Plt_P0_C1_R6_0 -> PT6_T5_d.loc = Plt_P0_C1_R6_0_ID;
	::f.PT6_T5_On_Plt_IntRAM0 -> PT6_T5_d.loc = Plt_IntRAM0_ID;
	dg;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD7_T6_d)
	cmd.nb_ins = 1;
	PT6_T5_d.id = 8;
	data_copy(cmd.outs[0], PT6_T5_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT6_T5_d, cmd.outs[0])
	PT6_T5!PT6_T5_d;
	t6_end = true;
};
}


bool d2_end = false;

active proctype input_D2(){ atomic {
	qualityLoss + 2;
	data in;
	in.id = 9;
	in.size = 512;
	gd
	::f.D2_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D2_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD2_T1!in;

	PD2_T3!in;
	d2_end = true;
};
}


bool t3_end = false;

active proctype T3(){ atomic {
	command cmd;

	byte proc = 0;
	byte pu = 0;
	data PD2_T3_d;
	data PT3_T4_d;
	PD2_T3?PD2_T3_d;
	gd
	::f.T3_On_Plt_P0_C0_P3 -> proc = Plt_P0_C0_P3_ID; pu = Plt_P0_C0_ID;
	::f.T3_On_Plt_P0_C1_P6 -> proc = Plt_P0_C1_P6_ID; pu = Plt_P0_C1_ID;
	dg;
	gd
	::f.PT3_T4_On_Plt_P0_C0_R3_0 -> PT3_T4_d.loc = Plt_P0_C0_R3_0_ID;
	::f.PT3_T4_On_Plt_IntRAM0 -> PT3_T4_d.loc = Plt_IntRAM0_ID;
	dg;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD2_T3_d)
	cmd.nb_ins = 1;
	PT3_T4_d.id = 10;
	data_copy(cmd.outs[0], PT3_T4_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT3_T4_d, cmd.outs[0])
	PT3_T4!PT3_T4_d;
	t3_end = true;
};
}


bool t4_end = false;

active proctype T4(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PT3_T4_d;
	PT3_T4?PT3_T4_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT3_T4_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t4_end = true;
};
}


bool t1_end = false;

active proctype T1(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P6_ID;
	byte pu = Plt_P0_C0_ID;
	data PD2_T1_d;
	data PT1_T5_d;
	data PT1_T0_d;
	PD2_T1?PD2_T1_d;
	PT1_T5_d.loc = Plt_IntRAM0_ID;
	gd
	::f.PT1_T0_On_Plt_P0_C0_R6_1 -> PT1_T0_d.loc = Plt_P0_C0_R6_1_ID;
	::f.PT1_T0_On_Plt_IntRAM0 -> PT1_T0_d.loc = Plt_IntRAM0_ID;
	dg;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD2_T1_d)
	cmd.nb_ins = 1;
	PT1_T5_d.id = 11;PT1_T5_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT1_T5_d)
	PT1_T0_d.id = 12;
	data_copy(cmd.outs[1], PT1_T0_d)
	cmd.nb_outs = 2;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT1_T5_d, cmd.outs[0])
	data_copy(PT1_T0_d, cmd.outs[1])
	PT1_T5!PT1_T5_d;
	PT1_T0!PT1_T0_d;
	t1_end = true;
};
}


bool t5_end = false;

active proctype T5(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C1_P5_ID;
	byte pu = Plt_P0_C1_ID;
	data PT1_T5_d;
	data PT6_T5_d;
	PT1_T5?PT1_T5_d;
	PT6_T5?PT6_T5_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT1_T5_d)
	data_copy(cmd.ins[1], PT6_T5_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t5_end = true;
};
}


bool t0_end = false;

active proctype T0(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PT1_T0_d;
	PT1_T0?PT1_T0_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT1_T0_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t0_end = true;
};
}

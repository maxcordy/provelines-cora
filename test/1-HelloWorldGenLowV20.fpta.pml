check <> (t0_end && t1_end && d2_end && t3_end && d4_end && d5_end && t6_end && t7_end && d8_end && t9_end && d10_end && t11_end && plt_introm0_idle && plt_intram0_idle && plt_p0_c0_idle) within 144000
typedef features{
bool DesignSpaceVariability;
bool ApplicationVariability;
bool D4;
bool T7;
bool T9;
bool PlatformVariability;
bool Plt_P0;
bool DeploymentVariability;
bool D5_On;
bool D5_On_Plt_IntROM0;
bool D5_On_Plt_IntRAM0;
bool D10_On;
bool D10_On_Plt_IntROM0;
bool D10_On_Plt_IntRAM0;
bool D8_On;
bool D8_On_Plt_IntROM0;
bool D8_On_Plt_IntRAM0;
bool D4_On;
bool D4_On_Plt_IntROM0;
bool D4_On_Plt_IntRAM0;
bool D2_On;
bool D2_On_Plt_IntROM0;
bool D2_On_Plt_IntRAM0;
bool PD2_T1_On;
bool PD2_T1_On_Plt_IntROM0;
bool PD2_T1_On_Plt_IntRAM0;
bool PD2_T3_On;
bool PD2_T3_On_Plt_IntROM0;
bool PD2_T3_On_Plt_IntRAM0;
bool PT1_T0_On;
bool PT1_T0_On_Plt_IntRAM0;
bool PD8_T7_On;
bool PD8_T7_On_Plt_IntROM0;
bool PD8_T7_On_Plt_IntRAM0;
bool PD10_T11_On;
bool PD10_T11_On_Plt_IntROM0;
bool PD10_T11_On_Plt_IntRAM0;
bool PD4_T3_On;
bool PD4_T3_On_Plt_IntROM0;
bool PD4_T3_On_Plt_IntRAM0;
bool PT0_T6_On;
bool PT0_T6_On_Plt_IntRAM0;
bool PD8_T9_On;
bool PD8_T9_On_Plt_IntROM0;
bool PD8_T9_On_Plt_IntRAM0;
bool PD10_T9_On;
bool PD10_T9_On_Plt_IntROM0;
bool PD10_T9_On_Plt_IntRAM0;
bool PT6_T7_On;
bool PT6_T7_On_Plt_IntRAM0;
bool PD5_T0_On;
bool PD5_T0_On_Plt_IntROM0;
bool PD5_T0_On_Plt_IntRAM0;
bool T1_On;
bool T1_On_Plt_P0_C0_P1;
bool T7_On;
bool T7_On_Plt_P0_C0_P8;
bool T9_On;
bool T9_On_Plt_P0_C0_P6;
bool T0_On;
bool T0_On_Plt_P0_C0_P9;
bool T6_On;
bool T6_On_Plt_P0_C0_P5;
bool T11_On;
bool T11_On_Plt_P0_C0_P0;
bool T3_On;
bool T3_On_Plt_P0_C0_P8
}features f;
//design number = 32.0
#define main_freq 100
#define burst 512

#define NB_MEMORY 11
#define NULL_MEMORY -1
#define Plt_P0_C0_R1_0_ID 0
#define Plt_P0_C0_R2_0_ID 1
#define Plt_P0_C0_R2_1_ID 2
#define Plt_P0_C0_R2_2_ID 3
#define Plt_P0_C0_R2_3_ID 4
#define Plt_P0_C0_R5_0_ID 5
#define Plt_P0_C0_R7_0_ID 6
#define Plt_P0_C0_R7_1_ID 7
#define Plt_P0_C0_R9_0_ID 8
#define Plt_IntROM0_ID 9
#define Plt_IntRAM0_ID 10


#define NB_PROCESSOR 10
#define NULL_PROCESSOR -1
#define Plt_P0_C0_P0_ID 0
#define Plt_P0_C0_P1_ID 1
#define Plt_P0_C0_P2_ID 2
#define Plt_P0_C0_P3_ID 3
#define Plt_P0_C0_P4_ID 4
#define Plt_P0_C0_P5_ID 5
#define Plt_P0_C0_P6_ID 6
#define Plt_P0_C0_P7_ID 7
#define Plt_P0_C0_P8_ID 8
#define Plt_P0_C0_P9_ID 9

#define NB_PU 1
#define NULL_PU -1
#define Plt_P0_C0_ID 0
typedef data{
	byte id;
	byte loc;
	int size
}

#define MAX_NB_INS 2
#define MAX_NB_OUTS 2

typedef command{
	byte fct;
	data ins[MAX_NB_INS];
	byte nb_ins;
	data outs[MAX_NB_OUTS];
	byte nb_outs
}

#define NB_TASK 7

#define LIST_LENGTH NB_TASK

typedef dependency{
	byte tasks[NB_TASK];
	byte depth
}

typedef command_list{
	command cmd[LIST_LENGTH];
	dependency dep[LIST_LENGTH];
	int size
}

chan transfer[NB_MEMORY] = [0] of {data};
chan send[NB_PU] = [0] of {command};
chan recv[NB_PU] = [0] of {command};



bool plt_introm0_idle = true; 

active proctype storage_Plt_IntROM0(){ atomic {
	int freq = 100;
	int bpc = 2;
	int latency =2;
	int _delay = 0;
	data in;
	freq = 100;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_IntROM0_ID]?in -> plt_introm0_idle = false; 
		wait(_delay) then skip;
		plt_introm0_idle = true; 
	od;
};
}

bool plt_intram0_idle = true; 

active proctype storage_Plt_IntRAM0(){ atomic {
	int freq = 100;
	int bpc = 2;
	int latency =2;
	int _delay = 0;
	data in;
	freq = 100;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_IntRAM0_ID]?in -> plt_intram0_idle = false; 
		wait(_delay) then skip;
		plt_intram0_idle = true; 
	od;
};
}
#define data_copy(dst, src) dst.id = src.id; dst.loc = src.loc; dst.size = src.size;

#define cmd_copy(dst, src) dst.fct = src.fct; dst.nb_ins = src.nb_ins; dst.nb_outs = src.nb_outs; \
	j = 0;																\
	do																	\
	:: j < src.nb_ins -> data_copy(dst.ins[j], src.ins[j]) j++			\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < src.nb_outs -> data_copy(dst.outs[j], src.outs[j]) j++		\
	:: else -> break;													\
	od;

#define free_data(data) data.id = 0; data.loc = 0; data.size = 0;

#define free_cmd(cmd)  \
	j = 0;																\
	do																	\
	:: j < cmd.nb_ins -> free_data(cmd.ins[j]) j++						\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < cmd.nb_outs -> free_data(cmd.outs[j]) j++					\
	:: else -> break;													\
	od;										\
	cmd.fct = 0; cmd.nb_ins = 0; cmd.nb_outs = 0;
	
#define is_external(id)	_is_external = id == Plt_IntROM0_ID || id == Plt_IntRAM0_ID;
#define inactive(cmd) _inactive = cmd.fct == 0 && cmd.nb_ins == 0 && cmd.nb_outs == 0;

#define declare_var()													\
	int i_cmd; int _delay;												\
	int i_time_cmd;														\
	int	i, j, k, l, m;													\
	bool _is_flush_cmd, _time_cmd, _hasCompleted, _is_external, _inactive;\
	command_list list;													\

#define allocate() 													\
	i = 0;																\
	do																	\
	::i < list.size ->													\
		inactive(list.cmd[i])											\
		if																\
		::_inactive -> i_cmd = i; break;								\
		::else -> skip;													\
		fi;																\
		i++;															\
	::else -> i_cmd = list.size; list.size++; break;					\
	od;																	\
	cmd_copy(list.cmd[i_cmd], cmd)										\
	list.dep[i_cmd].tasks[0] = i_cmd; 									\
	list.dep[i_cmd].depth = 1;

#define cmd_list_push()												\
	allocate()															\
	j = 0;																\
	do																	\
	::j < cmd.nb_ins ->													\
		k = 0;															\
		do																\
		:: k < list.size ->												\
			l = 0;														\
			do															\
			:: l < list.cmd[k].nb_outs ->								\
				if 														\
				:: list.cmd[k].outs[l].id == cmd.ins[j].id -> m = 0;	\
					do													\
					:: m < list.dep[k].depth ->							\
						list.dep[i_cmd].tasks[list.dep[i_cmd].depth] = list.dep[k].tasks[m];\
						list.dep[i_cmd].depth++;						\
						m++;											\
					:: else -> break;									\
					od;													\
				:: else -> break;										\
				fi;														\
				l++;													\
			:: else -> break;											\
			od;															\
			k++;														\
		:: else -> break;												\
		od;																\
		j++;															\
	:: else -> break;													\
	od;

#define free_cmd_list()												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		free_cmd(list.cmd[k])											\
		i++;															\
	:: else -> list.dep[i_cmd].depth = 0; break;						\
	od;

#define is_flush_cmd()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		is_external(cmd.outs[i].loc)									\
		if 																\
		:: !_is_external -> _is_flush_cmd = false; break;				\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> _is_flush_cmd = true; break;								\
	od;


#define prog_pipeline()												\
	nb_ins = 0; nb_outs = 0;											\
	i_ins = 0; i_outs = 0;												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_ins ->									\
			is_external(list.cmd[k].ins[j].loc)							\
			if															\
			:: _is_external -> 											\
				fetch[nb_ins] = 0;										\
				data_copy(ins[nb_ins], list.cmd[k].ins[j]) nb_ins++;	\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_outs ->									\
			is_external(list.cmd[k].outs[j].loc)						\
			if															\
			:: _is_external -> 											\
				store[nb_outs] = 0;										\
				data_copy(outs[nb_outs], list.cmd[k].outs[j]) nb_outs++;\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		i++;															\
	:: else -> break;													\
	od;

#define hasCompleted()													\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		:: fetch[i] < ins[i].size -> _hasCompleted = false; break;		\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> _hasCompleted = true; break;								\
	od;																	\
	i = 0;																\
	do																	\
	:: i < nb_outs && _hasCompleted ->									\
		if																\
		:: store[i] < outs[i].size -> _hasCompleted = false; break; 	\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> break;													\
	od;

#define checkIns()														\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		::fetch[i] >= ins[i].size -> nb_ins--;							\
			data_copy(ins[i], ins[nb_ins])								\
			fetch[i] = fetch[nb_ins];									\
			fetch[nb_ins] = 0;											\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> i_ins = 0; break;										\
	od;

#define checkOuts()														\
	i = 0;																\
	do																	\
	:: i < nb_outs ->													\
		if																\
		::store[i] >= outs[i].size -> nb_outs--;						\
			data_copy(outs[i], outs[nb_outs])							\
			store[i] = store[nb_outs];									\
			store[nb_outs] = 0;											\
		::else -> skip;													\
		fi;																\
		i++;															\
	:: else -> i_outs = 0; break;										\
	od;

#define setOutputs()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		cmd.outs[i].size = cmd.ins[0].size;	i++;						\
	:: else -> break;													\
	od;

#define time_cmd(cmd)												\
	if																	\
	::cmd.fct == Plt_P0_C0_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P7_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P8_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P9_ID -> _time_cmd = 4;					\
	fi;
#define time_cmds()														\
	time_cmd(list.cmd[ list.dep[i_cmd].tasks[0] ])						\
	l = _time_cmd;														\
	i = 1;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		l = _time_cmd;													\
		time_cmd(list.cmd[k])											\
		i++;															\
		if																\
		::_time_cmd < l -> l = _time_cmd;								\
		:: else -> skip;												\
		fi;																\
	:: else -> break;													\
	od;																	\
	_delay = (burst/l)*(main_freq/freq);

bool plt_p0_c0_idle = true;

active proctype processor_Plt_P0_C0(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
do
	::send[Plt_P0_C0_ID]?cmd -> plt_p0_c0_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P0_C0_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P0_C0_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p0_c0_idle = true;
	od;
};
}

chan PT0_T6 = [0] of {data}
chan PT1_T0 = [0] of {data}
chan PD2_T3 = [0] of {data}
chan PD2_T1 = [0] of {data}
chan PD4_T3 = [0] of {data}
chan PD5_T0 = [0] of {data}
chan PT6_T7 = [0] of {data}
chan PD8_T9 = [0] of {data}
chan PD8_T7 = [0] of {data}
chan PD10_T11 = [0] of {data}
chan PD10_T9 = [0] of {data}

bool d10_end = false;

active proctype input_D10(){ atomic {
	data in;
	in.id = 1;
	in.size = 512;
	gd
	::f.D10_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D10_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD10_T11!in;

	PD10_T9!in;
	d10_end = true;
};
}


bool t11_end = false;

active proctype T11(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PD10_T11_d;
	PD10_T11?PD10_T11_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD10_T11_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t11_end = true;
};
}


bool d8_end = false;

active proctype input_D8(){ atomic {
	data in;
	in.id = 2;
	in.size = 512;
	gd
	::f.D8_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D8_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD8_T7!in;

	PD8_T9!in;
	d8_end = true;
};
}


bool t9_end = false;

active proctype T9(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C0_P6_ID;
	byte pu = Plt_P0_C0_ID;
	data PD8_T9_d;
	data PD10_T9_d;
	PD8_T9?PD8_T9_d;
	PD10_T9?PD10_T9_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD8_T9_d)
	data_copy(cmd.ins[1], PD10_T9_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t9_end = true;
};
}


bool d5_end = false;

active proctype input_D5(){ atomic {
	data in;
	in.id = 3;
	in.size = 1024;
	gd
	::f.D5_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D5_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD5_T0!in;
	d5_end = true;
};
}


bool d4_end = false;

active proctype input_D4(){ atomic {
	qualityLoss + 1;
	data in;
	in.id = 4;
	in.size = 256;
	gd
	::f.D4_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D4_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD4_T3!in;
	d4_end = true;
};
}


bool d2_end = false;

active proctype input_D2(){ atomic {
	data in;
	in.id = 5;
	in.size = 1024;
	gd
	::f.D2_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D2_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD2_T1!in;

	PD2_T3!in;
	d2_end = true;
};
}


bool t3_end = false;

active proctype T3(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P8_ID;
	byte pu = Plt_P0_C0_ID;
	data PD4_T3_d;
	data PD2_T3_d;
	PD4_T3?PD4_T3_d;
	PD2_T3?PD2_T3_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD4_T3_d)
	data_copy(cmd.ins[1], PD2_T3_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t3_end = true;
};
}


bool t1_end = false;

active proctype T1(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P1_ID;
	byte pu = Plt_P0_C0_ID;
	data PD2_T1_d;
	data PT1_T0_d;
	PD2_T1?PD2_T1_d;
	PT1_T0_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD2_T1_d)
	cmd.nb_ins = 1;
	PT1_T0_d.id = 6;PT1_T0_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT1_T0_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT1_T0_d, cmd.outs[0])
	PT1_T0!PT1_T0_d;
	t1_end = true;
};
}


bool t0_end = false;

active proctype T0(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P9_ID;
	byte pu = Plt_P0_C0_ID;
	data PT1_T0_d;
	data PD5_T0_d;
	data PT0_T6_d;
	PT1_T0?PT1_T0_d;
	PD5_T0?PD5_T0_d;
	PT0_T6_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT1_T0_d)
	data_copy(cmd.ins[1], PD5_T0_d)
	cmd.nb_ins = 2;
	PT0_T6_d.id = 7;PT0_T6_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT0_T6_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT0_T6_d, cmd.outs[0])
	PT0_T6!PT0_T6_d;
	t0_end = true;
};
}


bool t6_end = false;

active proctype T6(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P5_ID;
	byte pu = Plt_P0_C0_ID;
	data PT0_T6_d;
	data PT6_T7_d;
	PT0_T6?PT0_T6_d;
	PT6_T7_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT0_T6_d)
	cmd.nb_ins = 1;
	PT6_T7_d.id = 8;PT6_T7_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT6_T7_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT6_T7_d, cmd.outs[0])
	PT6_T7!PT6_T7_d;
	t6_end = true;
};
}


bool t7_end = false;

active proctype T7(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C0_P8_ID;
	byte pu = Plt_P0_C0_ID;
	data PD8_T7_d;
	data PT6_T7_d;
	PD8_T7?PD8_T7_d;
	PT6_T7?PT6_T7_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD8_T7_d)
	data_copy(cmd.ins[1], PT6_T7_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t7_end = true;
};
}

check <> (d0_end && t1_end && t2_end && t3_end && t4_end && t5_end && t6_end && t7_end && t8_end && t9_end && d10_end && t11_end && d12_end && t13_end && d14_end && t15_end && plt_intram0_idle && plt_extrom0_idle && plt_p0_c1_idle && plt_p0_c0_idle) within 192000
typedef features{
bool DesignSpaceVariability;
bool ApplicationVariability;
bool D0_size;
bool D0_size_512;
bool D0_size_1024;
bool T1;
bool T2;
bool T3;
bool T4;
bool T5;
bool T6;
bool T8;
bool T9;
bool D10;
bool D10_size;
bool D10_size_256;
bool D10_size_512;
bool D10_size_1024;
bool D12_size;
bool D12_size_256;
bool D12_size_512;
bool D12_size_1024;
bool T13;
bool D14;
bool D14_size;
bool D14_size_256;
bool D14_size_512;
bool D14_size_1024;
bool PT1_T2;
bool PT5_T4;
bool PT6_T5;
bool PFromT7_To;
bool PFromT7_To_T13;
bool PFromT7_To_T6;
bool PFromD0_To;
bool PFromD0_To_T1;
bool PFromD0_To_T3;
bool PlatformVariability;
bool Plt_P0;
bool Plt_IntRAM0;
bool Plt_IntRAM0_capacity;
bool Plt_IntRAM0_capacity_512;
bool Plt_IntRAM0_capacity_2048;
bool DeploymentVariability;
bool D12_On;
bool D12_On_Plt_ExtROM0;
bool D12_On_Plt_IntRAM0;
bool D0_On;
bool D0_On_Plt_ExtROM0;
bool D0_On_Plt_IntRAM0;
bool D14_On;
bool D14_On_Plt_ExtROM0;
bool D14_On_Plt_IntRAM0;
bool D10_On;
bool D10_On_Plt_ExtROM0;
bool D10_On_Plt_IntRAM0;
bool PT1_T2_On;
bool PT1_T2_On_Plt_IntRAM0;
bool PD12_T9_On;
bool PD12_T9_On_Plt_ExtROM0;
bool PD12_T9_On_Plt_IntRAM0;
bool PD10_T11_On;
bool PD10_T11_On_Plt_ExtROM0;
bool PD10_T11_On_Plt_IntRAM0;
bool PFromT7_On;
bool PFromT7_On_Plt_IntRAM0;
bool PFromD0_On;
bool PFromD0_On_Plt_ExtROM0;
bool PFromD0_On_Plt_IntRAM0;
bool PD14_T15_On;
bool PD14_T15_On_Plt_ExtROM0;
bool PD14_T15_On_Plt_IntRAM0;
bool PT9_T8_On;
bool PT9_T8_On_Plt_IntRAM0;
bool PT6_T5_On;
bool PT6_T5_On_Plt_IntRAM0;
bool PT8_T7_On;
bool PT8_T7_On_Plt_IntRAM0;
bool PD10_T9_On;
bool PD10_T9_On_Plt_ExtROM0;
bool PD10_T9_On_Plt_IntRAM0;
bool PT5_T4_On;
bool PT5_T4_On_Plt_IntRAM0;
bool T9_On;
bool T9_On_Plt_P0_C0_P2;
bool T1_On;
bool T1_On_Plt_P0_C0_P6;
bool T11_On;
bool T11_On_Plt_P0_C0_P0;
bool T4_On;
bool T4_On_Plt_P0_C0_P0;
bool T3_On;
bool T3_On_Plt_P0_C0_P0;
bool T7_On;
bool T7_On_Plt_P0_C0_P5;
bool T7_On_Plt_P0_C1_P1;
bool T2_On;
bool T2_On_Plt_P0_C1_P6;
bool T13_On;
bool T13_On_Plt_P0_C0_P0;
bool T8_On;
bool T8_On_Plt_P0_C0_P6;
bool T5_On;
bool T5_On_Plt_P0_C0_P5;
bool T5_On_Plt_P0_C1_P1;
bool T15_On;
bool T15_On_Plt_P0_C0_P0;
bool T6_On;
bool T6_On_Plt_P0_C0_P5;
bool T6_On_Plt_P0_C1_P1
}features f;
//design number = 34560.0
#define main_freq 200
#define burst 512

#define NB_MEMORY 16
#define NULL_MEMORY -1
#define Plt_P0_C0_R1_0_ID 0
#define Plt_P0_C0_R1_1_ID 1
#define Plt_P0_C0_R1_2_ID 2
#define Plt_P0_C0_R6_0_ID 3
#define Plt_P0_C0_R2_0_ID 4
#define Plt_P0_C0_R3_0_ID 5
#define Plt_P0_C0_R5_0_ID 6
#define Plt_P0_C1_R2_0_ID 7
#define Plt_P0_C1_R1_0_ID 8
#define Plt_P0_C1_R5_0_ID 9
#define Plt_P0_C1_R5_1_ID 10
#define Plt_P0_C1_R7_0_ID 11
#define Plt_P0_C1_R3_0_ID 12
#define Plt_P0_C1_R4_0_ID 13
#define Plt_IntRAM0_ID 14
#define Plt_ExtROM0_ID 15


#define NB_PROCESSOR 16
#define NULL_PROCESSOR -1
#define Plt_P0_C0_P0_ID 0
#define Plt_P0_C0_P1_ID 1
#define Plt_P0_C0_P2_ID 2
#define Plt_P0_C0_P3_ID 3
#define Plt_P0_C0_P4_ID 4
#define Plt_P0_C0_P5_ID 5
#define Plt_P0_C0_P6_ID 6
#define Plt_P0_C0_P7_ID 7
#define Plt_P0_C1_P0_ID 8
#define Plt_P0_C1_P1_ID 9
#define Plt_P0_C1_P2_ID 10
#define Plt_P0_C1_P3_ID 11
#define Plt_P0_C1_P4_ID 12
#define Plt_P0_C1_P5_ID 13
#define Plt_P0_C1_P6_ID 14
#define Plt_P0_C1_P7_ID 15

#define NB_PU 2
#define NULL_PU -1
#define Plt_P0_C1_ID 0
#define Plt_P0_C0_ID 1
typedef data{
	byte id;
	byte loc;
	int size
}

#define MAX_NB_INS 2
#define MAX_NB_OUTS 2

typedef command{
	byte fct;
	data ins[MAX_NB_INS];
	byte nb_ins;
	data outs[MAX_NB_OUTS];
	byte nb_outs
}

#define NB_TASK 12

#define LIST_LENGTH NB_TASK

typedef dependency{
	byte tasks[NB_TASK];
	byte depth
}

typedef command_list{
	command cmd[LIST_LENGTH];
	dependency dep[LIST_LENGTH];
	int size
}

chan transfer[NB_MEMORY] = [0] of {data};
chan send[NB_PU] = [0] of {command};
chan recv[NB_PU] = [0] of {command};



bool plt_intram0_idle = true; 

active proctype storage_Plt_IntRAM0(){ atomic {
	int freq = 200;
	int bpc = 2;
	int latency =2;
	int _delay = 0;
	data in;
	freq = 200;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_IntRAM0_ID]?in -> plt_intram0_idle = false; 
		wait(_delay) then skip;
		plt_intram0_idle = true; 
	od;
};
}

bool plt_extrom0_idle = true; 

active proctype storage_Plt_ExtROM0(){ atomic {
	int freq = 50;
	int bpc = 2;
	int latency =4;
	int _delay = 0;
	data in;
	freq = 50;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_ExtROM0_ID]?in -> plt_extrom0_idle = false; 
		wait(_delay) then skip;
		plt_extrom0_idle = true; 
	od;
};
}
#define data_copy(dst, src) dst.id = src.id; dst.loc = src.loc; dst.size = src.size;

#define cmd_copy(dst, src) dst.fct = src.fct; dst.nb_ins = src.nb_ins; dst.nb_outs = src.nb_outs; \
	j = 0;																\
	do																	\
	:: j < src.nb_ins -> data_copy(dst.ins[j], src.ins[j]) j++			\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < src.nb_outs -> data_copy(dst.outs[j], src.outs[j]) j++		\
	:: else -> break;													\
	od;

#define free_data(data) data.id = 0; data.loc = 0; data.size = 0;

#define free_cmd(cmd)  \
	j = 0;																\
	do																	\
	:: j < cmd.nb_ins -> free_data(cmd.ins[j]) j++						\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < cmd.nb_outs -> free_data(cmd.outs[j]) j++					\
	:: else -> break;													\
	od;										\
	cmd.fct = 0; cmd.nb_ins = 0; cmd.nb_outs = 0;
	
#define is_external(id)	_is_external = id == Plt_IntRAM0_ID || id == Plt_ExtROM0_ID;
#define inactive(cmd) _inactive = cmd.fct == 0 && cmd.nb_ins == 0 && cmd.nb_outs == 0;

#define declare_var()													\
	int i_cmd; int _delay;												\
	int i_time_cmd;														\
	int	i, j, k, l, m;													\
	bool _is_flush_cmd, _time_cmd, _hasCompleted, _is_external, _inactive;\
	command_list list;													\

#define allocate() 													\
	i = 0;																\
	do																	\
	::i < list.size ->													\
		inactive(list.cmd[i])											\
		if																\
		::_inactive -> i_cmd = i; break;								\
		::else -> skip;													\
		fi;																\
		i++;															\
	::else -> i_cmd = list.size; list.size++; break;					\
	od;																	\
	cmd_copy(list.cmd[i_cmd], cmd)										\
	list.dep[i_cmd].tasks[0] = i_cmd; 									\
	list.dep[i_cmd].depth = 1;

#define cmd_list_push()												\
	allocate()															\
	j = 0;																\
	do																	\
	::j < cmd.nb_ins ->													\
		k = 0;															\
		do																\
		:: k < list.size ->												\
			l = 0;														\
			do															\
			:: l < list.cmd[k].nb_outs ->								\
				if 														\
				:: list.cmd[k].outs[l].id == cmd.ins[j].id -> m = 0;	\
					do													\
					:: m < list.dep[k].depth ->							\
						list.dep[i_cmd].tasks[list.dep[i_cmd].depth] = list.dep[k].tasks[m];\
						list.dep[i_cmd].depth++;						\
						m++;											\
					:: else -> break;									\
					od;													\
				:: else -> break;										\
				fi;														\
				l++;													\
			:: else -> break;											\
			od;															\
			k++;														\
		:: else -> break;												\
		od;																\
		j++;															\
	:: else -> break;													\
	od;

#define free_cmd_list()												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		free_cmd(list.cmd[k])											\
		i++;															\
	:: else -> list.dep[i_cmd].depth = 0; break;						\
	od;

#define is_flush_cmd()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		is_external(cmd.outs[i].loc)									\
		if 																\
		:: !_is_external -> _is_flush_cmd = false; break;				\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> _is_flush_cmd = true; break;								\
	od;


#define prog_pipeline()												\
	nb_ins = 0; nb_outs = 0;											\
	i_ins = 0; i_outs = 0;												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_ins ->									\
			is_external(list.cmd[k].ins[j].loc)							\
			if															\
			:: _is_external -> 											\
				fetch[nb_ins] = 0;										\
				data_copy(ins[nb_ins], list.cmd[k].ins[j]) nb_ins++;	\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_outs ->									\
			is_external(list.cmd[k].outs[j].loc)						\
			if															\
			:: _is_external -> 											\
				store[nb_outs] = 0;										\
				data_copy(outs[nb_outs], list.cmd[k].outs[j]) nb_outs++;\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		i++;															\
	:: else -> break;													\
	od;

#define hasCompleted()													\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		:: fetch[i] < ins[i].size -> _hasCompleted = false; break;		\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> _hasCompleted = true; break;								\
	od;																	\
	i = 0;																\
	do																	\
	:: i < nb_outs && _hasCompleted ->									\
		if																\
		:: store[i] < outs[i].size -> _hasCompleted = false; break; 	\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> break;													\
	od;

#define checkIns()														\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		::fetch[i] >= ins[i].size -> nb_ins--;							\
			data_copy(ins[i], ins[nb_ins])								\
			fetch[i] = fetch[nb_ins];									\
			fetch[nb_ins] = 0;											\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> i_ins = 0; break;										\
	od;

#define checkOuts()														\
	i = 0;																\
	do																	\
	:: i < nb_outs ->													\
		if																\
		::store[i] >= outs[i].size -> nb_outs--;						\
			data_copy(outs[i], outs[nb_outs])							\
			store[i] = store[nb_outs];									\
			store[nb_outs] = 0;											\
		::else -> skip;													\
		fi;																\
		i++;															\
	:: else -> i_outs = 0; break;										\
	od;

#define setOutputs()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		cmd.outs[i].size = cmd.ins[0].size;	i++;						\
	:: else -> break;													\
	od;

#define time_cmd(cmd)												\
	if																	\
	::cmd.fct == Plt_P0_C0_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P7_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P7_ID -> _time_cmd = 4;					\
	fi;
#define time_cmds()														\
	time_cmd(list.cmd[ list.dep[i_cmd].tasks[0] ])						\
	l = _time_cmd;														\
	i = 1;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		l = _time_cmd;													\
		time_cmd(list.cmd[k])											\
		i++;															\
		if																\
		::_time_cmd < l -> l = _time_cmd;								\
		:: else -> skip;												\
		fi;																\
	:: else -> break;													\
	od;																	\
	_delay = (burst/l)*(main_freq/freq);

bool plt_p0_c1_idle = true;

active proctype processor_Plt_P0_C1(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
do
	::send[Plt_P0_C1_ID]?cmd -> plt_p0_c1_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P0_C1_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P0_C1_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p0_c1_idle = true;
	od;
};
}

bool plt_p0_c0_idle = true;

active proctype processor_Plt_P0_C0(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
do
	::send[Plt_P0_C0_ID]?cmd -> plt_p0_c0_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P0_C0_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P0_C0_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p0_c0_idle = true;
	od;
};
}

chan PFromD0 = [0] of {data}
chan PT1_T2 = [0] of {data}
chan PT5_T4 = [0] of {data}
chan PT6_T5 = [0] of {data}
chan PFromT7 = [0] of {data}
chan PT8_T7 = [0] of {data}
chan PT9_T8 = [0] of {data}
chan PD10_T11 = [0] of {data}
chan PD10_T9 = [0] of {data}
chan PD12_T9 = [0] of {data}
chan PD14_T15 = [0] of {data}

bool d14_end = false;

active proctype input_D14(){ atomic {
	qualityLoss + 1;
	data in;
	in.id = 1;
	gd
	::f.D14_size_256->  in.size = 256;
	::f.D14_size_512-> 
qualityLoss + 1; in.size = 512;
	::f.D14_size_1024->  in.size = 1024;
	dg;
	gd
	::f.D14_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D14_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD14_T15!in;
	d14_end = true;
};
}


bool t15_end = false;

active proctype T15(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PD14_T15_d;
	PD14_T15?PD14_T15_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD14_T15_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t15_end = true;
};
}


bool d12_end = false;

active proctype input_D12(){ atomic {
	data in;
	in.id = 2;
	gd
	::f.D12_size_256->  in.size = 256;
	::f.D12_size_512-> 
qualityLoss + 1; in.size = 512;
	::f.D12_size_1024->  in.size = 1024;
	dg;
	gd
	::f.D12_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D12_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD12_T9!in;
	d12_end = true;
};
}


bool d10_end = false;

active proctype input_D10(){ atomic {
	qualityLoss + 1;
	data in;
	in.id = 3;
	gd
	::f.D10_size_256->  in.size = 256;
	::f.D10_size_512-> 
qualityLoss + 1; in.size = 512;
	::f.D10_size_1024->  in.size = 1024;
	dg;
	gd
	::f.D10_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D10_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD10_T11!in;

	PD10_T9!in;
	d10_end = true;
};
}


bool t11_end = false;

active proctype T11(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PD10_T11_d;
	PD10_T11?PD10_T11_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD10_T11_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t11_end = true;
};
}


bool t9_end = false;

active proctype T9(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P0_C0_P2_ID;
	byte pu = Plt_P0_C0_ID;
	data PD12_T9_d;
	data PD10_T9_d;
	data PT9_T8_d;
	PD12_T9?PD12_T9_d;
	PD10_T9?PD10_T9_d;
	PT9_T8_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD12_T9_d)
	data_copy(cmd.ins[1], PD10_T9_d)
	cmd.nb_ins = 2;
	PT9_T8_d.id = 4;PT9_T8_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT9_T8_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT9_T8_d, cmd.outs[0])
	PT9_T8!PT9_T8_d;
	t9_end = true;
};
}


bool t8_end = false;

active proctype T8(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P0_C0_P6_ID;
	byte pu = Plt_P0_C0_ID;
	data PT9_T8_d;
	data PT8_T7_d;
	PT9_T8?PT9_T8_d;
	PT8_T7_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT9_T8_d)
	cmd.nb_ins = 1;
	PT8_T7_d.id = 5;PT8_T7_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT8_T7_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT8_T7_d, cmd.outs[0])
	PT8_T7!PT8_T7_d;
	t8_end = true;
};
}


bool t7_end = false;

active proctype T7(){ atomic {
	command cmd;

	byte proc = 0;
	byte pu = 0;
	data PT8_T7_d;
	data PFromT7_d;
	PT8_T7?PT8_T7_d;
	gd
	::f.T7_On_Plt_P0_C0_P5 -> proc = Plt_P0_C0_P5_ID; pu = Plt_P0_C0_ID;
	::f.T7_On_Plt_P0_C1_P1 -> proc = Plt_P0_C1_P1_ID; pu = Plt_P0_C1_ID;
	dg;
	PFromT7_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT8_T7_d)
	cmd.nb_ins = 1;
	PFromT7_d.id = 6;PFromT7_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PFromT7_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PFromT7_d, cmd.outs[0])
	PFromT7!PFromT7_d;
	t7_end = true;
};
}


bool t13_end = false;

active proctype T13(){ atomic {
gd::f.T13->

	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PFromT7_d;
	PFromT7?PFromT7_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PFromT7_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
::else -> skip;
dg;
	t13_end = true;
};
}


bool t6_end = false;

active proctype T6(){ atomic {
gd::f.T6->

	command cmd;

	byte proc = 0;
	byte pu = 0;
	data PFromT7_d;
	data PT6_T5_d;
	PFromT7?PFromT7_d;
	gd
	::f.T6_On_Plt_P0_C0_P5 -> proc = Plt_P0_C0_P5_ID; pu = Plt_P0_C0_ID;
	::f.T6_On_Plt_P0_C1_P1 -> proc = Plt_P0_C1_P1_ID; pu = Plt_P0_C1_ID;
	dg;
	PT6_T5_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PFromT7_d)
	cmd.nb_ins = 1;
	PT6_T5_d.id = 7;PT6_T5_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT6_T5_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT6_T5_d, cmd.outs[0])
	PT6_T5!PT6_T5_d;
::else -> skip;
dg;
	t6_end = true;
};
}


bool t5_end = false;

active proctype T5(){ atomic {
gd::f.T5->

	command cmd;

	byte proc = 0;
	byte pu = 0;
	data PT6_T5_d;
	data PT5_T4_d;
	PT6_T5?PT6_T5_d;
	gd
	::f.T5_On_Plt_P0_C0_P5 -> proc = Plt_P0_C0_P5_ID; pu = Plt_P0_C0_ID;
	::f.T5_On_Plt_P0_C1_P1 -> proc = Plt_P0_C1_P1_ID; pu = Plt_P0_C1_ID;
	dg;
	PT5_T4_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT6_T5_d)
	cmd.nb_ins = 1;
	PT5_T4_d.id = 8;PT5_T4_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT5_T4_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT5_T4_d, cmd.outs[0])
	PT5_T4!PT5_T4_d;
::else -> skip;
dg;
	t5_end = true;
};
}


bool t4_end = false;

active proctype T4(){ atomic {
gd::f.T4->

	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PT5_T4_d;
	PT5_T4?PT5_T4_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT5_T4_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
::else -> skip;
dg;
	t4_end = true;
};
}


bool d0_end = false;

active proctype input_D0(){ atomic {
	data in;
	in.id = 9;
	gd
	::f.D0_size_512->  in.size = 512;
	::f.D0_size_1024->  in.size = 1024;
	dg;
	gd
	::f.D0_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D0_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PFromD0!in;
	d0_end = true;
};
}


bool t3_end = false;

active proctype T3(){ atomic {
gd::f.T3->

	command cmd;

	byte proc = Plt_P0_C0_P0_ID;
	byte pu = Plt_P0_C0_ID;
	data PFromD0_d;
	PFromD0?PFromD0_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PFromD0_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
::else -> skip;
dg;
	t3_end = true;
};
}


bool t1_end = false;

active proctype T1(){ atomic {
gd::f.T1->

	command cmd;

	byte proc = Plt_P0_C0_P6_ID;
	byte pu = Plt_P0_C0_ID;
	data PFromD0_d;
	data PT1_T2_d;
	PFromD0?PFromD0_d;
	PT1_T2_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PFromD0_d)
	cmd.nb_ins = 1;
	PT1_T2_d.id = 10;PT1_T2_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT1_T2_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT1_T2_d, cmd.outs[0])
	PT1_T2!PT1_T2_d;
::else -> skip;
dg;
	t1_end = true;
};
}


bool t2_end = false;

active proctype T2(){ atomic {
gd::f.T2->

	command cmd;

	byte proc = Plt_P0_C1_P6_ID;
	byte pu = Plt_P0_C1_ID;
	data PT1_T2_d;
	PT1_T2?PT1_T2_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT1_T2_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
::else -> skip;
dg;
	t2_end = true;
};
}

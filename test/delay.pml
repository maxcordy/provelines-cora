check <> (end == true) within 10

// Declare list of features inside the special type "features"
typedef features {
   bool Foo;
   bool Bar
};

// Declare an instance of this type
features f;

bool end = false;


active proctype toto() {
   int i = 0;
   int delay = 0;

   // Increment i only if feature Foo or feature Bar is selected.
   gd :: f.Foo -> cost + 2, qualityLoss + 4; delay = 10; 
	 	  i++;
      :: else -> skip;
   dg;

   wait (delay) then skip;
   delay = 0;

   gd :: f.Bar -> delay = 5; 
		  i++;
      :: else -> skip;
   dg;

   wait (delay) then skip;

   // Test assertion
   //assert(i > 0);
   end = true;

   do :: skip; od;
}
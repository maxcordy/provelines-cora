check <> (t0_end && d1_end && t2_end && t3_end && t4_end && t5_end && t6_end && d7_end && t8_end && t9_end && d10_end && t11_end && t12_end && t13_end && t14_end && t15_end && t16_end && d17_end && t18_end && d19_end && plt_introm0_idle && plt_intram0_idle && plt_extrom0_idle && plt_p0_c0_idle && plt_p1_c0_idle && plt_p0_c1_idle && plt_p1_c1_idle) within 240000
typedef features{
bool DesignSpaceVariability;
bool ApplicationVariability;
bool D1;
bool T4;
bool T6;
bool T9;
bool D10;
bool T11;
bool T12;
bool T13;
bool T15;
bool T16;
bool D19;
bool PlatformVariability;
bool Plt_P0;
bool Plt_P1;
bool DeploymentVariability;
bool D17_On;
bool D17_On_Plt_IntROM0;
bool D17_On_Plt_ExtROM0;
bool D17_On_Plt_IntRAM0;
bool D10_On;
bool D10_On_Plt_IntROM0;
bool D10_On_Plt_ExtROM0;
bool D10_On_Plt_IntRAM0;
bool D1_On;
bool D1_On_Plt_IntROM0;
bool D1_On_Plt_ExtROM0;
bool D1_On_Plt_IntRAM0;
bool D19_On;
bool D19_On_Plt_IntROM0;
bool D19_On_Plt_ExtROM0;
bool D19_On_Plt_IntRAM0;
bool D7_On;
bool D7_On_Plt_IntROM0;
bool D7_On_Plt_ExtROM0;
bool D7_On_Plt_IntRAM0;
bool PD19_T18_On;
bool PD19_T18_On_Plt_IntROM0;
bool PD19_T18_On_Plt_ExtROM0;
bool PD19_T18_On_Plt_IntRAM0;
bool PT15_T14_On;
bool PT15_T14_On_Plt_IntRAM0;
bool PT13_T12_On;
bool PT13_T12_On_Plt_IntRAM0;
bool PT11_T12_On;
bool PT11_T12_On_Plt_IntRAM0;
bool PD7_T8_On;
bool PD7_T8_On_Plt_IntROM0;
bool PD7_T8_On_Plt_ExtROM0;
bool PD7_T8_On_Plt_IntRAM0;
bool PD10_T5_On;
bool PD10_T5_On_Plt_IntROM0;
bool PD10_T5_On_Plt_ExtROM0;
bool PD10_T5_On_Plt_IntRAM0;
bool PT14_T13_On;
bool PT14_T13_On_Plt_IntRAM0;
bool PD17_T16_On;
bool PD17_T16_On_Plt_IntROM0;
bool PD17_T16_On_Plt_ExtROM0;
bool PD17_T16_On_Plt_IntRAM0;
bool PT16_T15_On;
bool PT16_T15_On_Plt_IntRAM0;
bool PT6_T5_On;
bool PT6_T5_On_Plt_IntRAM0;
bool PD17_T18_On;
bool PD17_T18_On_Plt_IntROM0;
bool PD17_T18_On_Plt_ExtROM0;
bool PD17_T18_On_Plt_IntRAM0;
bool PT3_T2_On;
bool PT3_T2_On_Plt_IntRAM0;
bool PT4_T3_On;
bool PT4_T3_On_Plt_IntRAM0;
bool PD10_T11_On;
bool PD10_T11_On_Plt_IntROM0;
bool PD10_T11_On_Plt_ExtROM0;
bool PD10_T11_On_Plt_IntRAM0;
bool PT0_T2_On;
bool PT0_T2_On_Plt_IntRAM0;
bool PD7_T6_On;
bool PD7_T6_On_Plt_IntROM0;
bool PD7_T6_On_Plt_ExtROM0;
bool PD7_T6_On_Plt_IntRAM0;
bool PD1_T0_On;
bool PD1_T0_On_Plt_IntROM0;
bool PD1_T0_On_Plt_ExtROM0;
bool PD1_T0_On_Plt_IntRAM0;
bool PT6_T9_On;
bool PT6_T9_On_Plt_IntRAM0;
bool PT5_T4_On;
bool PT5_T4_On_Plt_IntRAM0;
bool T5_On;
bool T5_On_Plt_P1_C1_P6;
bool T8_On;
bool T8_On_Plt_P0_C0_P7;
bool T3_On;
bool T3_On_Plt_P1_C0_P5;
bool T6_On;
bool T6_On_Plt_P0_C0_P5;
bool T0_On;
bool T0_On_Plt_P0_C0_P1;
bool T4_On;
bool T4_On_Plt_P0_C1_P1;
bool T14_On;
bool T14_On_Plt_P0_C0_P1;
bool T11_On;
bool T11_On_Plt_P1_C0_P5;
bool T18_On;
bool T18_On_Plt_P1_C1_P3;
bool T15_On;
bool T15_On_Plt_P0_C1_P1;
bool T2_On;
bool T2_On_Plt_P1_C1_P3;
bool T12_On;
bool T12_On_Plt_P1_C1_P3;
bool T13_On;
bool T13_On_Plt_P0_C1_P1;
bool T9_On;
bool T9_On_Plt_P1_C1_P0;
bool T16_On;
bool T16_On_Plt_P0_C1_P1
}features f;
//design number = 243.0
#define main_freq 200
#define burst 512

#define NB_MEMORY 31
#define NULL_MEMORY -1
#define Plt_P0_C0_R3_0_ID 0
#define Plt_P0_C0_R2_0_ID 1
#define Plt_P0_C0_R2_1_ID 2
#define Plt_P0_C0_R6_0_ID 3
#define Plt_P0_C0_R5_0_ID 4
#define Plt_P0_C0_R5_1_ID 5
#define Plt_P0_C0_R1_0_ID 6
#define Plt_P0_C1_R5_0_ID 7
#define Plt_P0_C1_R1_0_ID 8
#define Plt_P0_C1_R6_0_ID 9
#define Plt_P0_C1_R6_1_ID 10
#define Plt_P0_C1_R2_0_ID 11
#define Plt_P0_C1_R2_1_ID 12
#define Plt_P0_C1_R3_0_ID 13
#define Plt_P1_C0_R5_0_ID 14
#define Plt_P1_C0_R1_0_ID 15
#define Plt_P1_C0_R1_1_ID 16
#define Plt_P1_C0_R6_0_ID 17
#define Plt_P1_C0_R6_1_ID 18
#define Plt_P1_C0_R3_0_ID 19
#define Plt_P1_C0_R4_0_ID 20
#define Plt_P1_C1_R6_0_ID 21
#define Plt_P1_C1_R4_0_ID 22
#define Plt_P1_C1_R4_1_ID 23
#define Plt_P1_C1_R7_0_ID 24
#define Plt_P1_C1_R1_0_ID 25
#define Plt_P1_C1_R1_1_ID 26
#define Plt_P1_C1_R2_0_ID 27
#define Plt_IntROM0_ID 28
#define Plt_IntRAM0_ID 29
#define Plt_ExtROM0_ID 30


#define NB_PROCESSOR 32
#define NULL_PROCESSOR -1
#define Plt_P0_C0_P0_ID 0
#define Plt_P0_C0_P1_ID 1
#define Plt_P0_C0_P2_ID 2
#define Plt_P0_C0_P3_ID 3
#define Plt_P0_C0_P4_ID 4
#define Plt_P0_C0_P5_ID 5
#define Plt_P0_C0_P6_ID 6
#define Plt_P0_C0_P7_ID 7
#define Plt_P0_C1_P0_ID 8
#define Plt_P0_C1_P1_ID 9
#define Plt_P0_C1_P2_ID 10
#define Plt_P0_C1_P3_ID 11
#define Plt_P0_C1_P4_ID 12
#define Plt_P0_C1_P5_ID 13
#define Plt_P0_C1_P6_ID 14
#define Plt_P0_C1_P7_ID 15
#define Plt_P1_C0_P0_ID 16
#define Plt_P1_C0_P1_ID 17
#define Plt_P1_C0_P2_ID 18
#define Plt_P1_C0_P3_ID 19
#define Plt_P1_C0_P4_ID 20
#define Plt_P1_C0_P5_ID 21
#define Plt_P1_C0_P6_ID 22
#define Plt_P1_C0_P7_ID 23
#define Plt_P1_C1_P0_ID 24
#define Plt_P1_C1_P1_ID 25
#define Plt_P1_C1_P2_ID 26
#define Plt_P1_C1_P3_ID 27
#define Plt_P1_C1_P4_ID 28
#define Plt_P1_C1_P5_ID 29
#define Plt_P1_C1_P6_ID 30
#define Plt_P1_C1_P7_ID 31

#define NB_PU 4
#define NULL_PU -1
#define Plt_P0_C0_ID 0
#define Plt_P1_C0_ID 1
#define Plt_P0_C1_ID 2
#define Plt_P1_C1_ID 3
typedef data{
	byte id;
	byte loc;
	int size
}

#define MAX_NB_INS 2
#define MAX_NB_OUTS 2

typedef command{
	byte fct;
	data ins[MAX_NB_INS];
	byte nb_ins;
	data outs[MAX_NB_OUTS];
	byte nb_outs
}

#define NB_TASK 15

#define LIST_LENGTH NB_TASK

typedef dependency{
	byte tasks[NB_TASK];
	byte depth
}

typedef command_list{
	command cmd[LIST_LENGTH];
	dependency dep[LIST_LENGTH];
	int size
}

chan transfer[NB_MEMORY] = [0] of {data};
chan send[NB_PU] = [0] of {command};
chan recv[NB_PU] = [0] of {command};



bool plt_introm0_idle = true; 

active proctype storage_Plt_IntROM0(){ atomic {
	int freq = 200;
	int bpc = 2;
	int latency =2;
	int _delay = 0;
	data in;
	freq = 200;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_IntROM0_ID]?in -> plt_introm0_idle = false; 
		wait(_delay) then skip;
		plt_introm0_idle = true; 
	od;
};
}

bool plt_intram0_idle = true; 

active proctype storage_Plt_IntRAM0(){ atomic {
	int freq = 200;
	int bpc = 2;
	int latency =2;
	int _delay = 0;
	data in;
	freq = 200;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_IntRAM0_ID]?in -> plt_intram0_idle = false; 
		wait(_delay) then skip;
		plt_intram0_idle = true; 
	od;
};
}

bool plt_extrom0_idle = true; 

active proctype storage_Plt_ExtROM0(){ atomic {
	int freq = 25;
	int bpc = 2;
	int latency =4;
	int _delay = 0;
	data in;
	freq = 25;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[Plt_ExtROM0_ID]?in -> plt_extrom0_idle = false; 
		wait(_delay) then skip;
		plt_extrom0_idle = true; 
	od;
};
}
#define data_copy(dst, src) dst.id = src.id; dst.loc = src.loc; dst.size = src.size;

#define cmd_copy(dst, src) dst.fct = src.fct; dst.nb_ins = src.nb_ins; dst.nb_outs = src.nb_outs; \
	j = 0;																\
	do																	\
	:: j < src.nb_ins -> data_copy(dst.ins[j], src.ins[j]) j++			\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < src.nb_outs -> data_copy(dst.outs[j], src.outs[j]) j++		\
	:: else -> break;													\
	od;

#define free_data(data) data.id = 0; data.loc = 0; data.size = 0;

#define free_cmd(cmd)  \
	j = 0;																\
	do																	\
	:: j < cmd.nb_ins -> free_data(cmd.ins[j]) j++						\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < cmd.nb_outs -> free_data(cmd.outs[j]) j++					\
	:: else -> break;													\
	od;										\
	cmd.fct = 0; cmd.nb_ins = 0; cmd.nb_outs = 0;
	
#define is_external(id)	_is_external = id == Plt_IntROM0_ID || id == Plt_IntRAM0_ID || id == Plt_ExtROM0_ID;
#define inactive(cmd) _inactive = cmd.fct == 0 && cmd.nb_ins == 0 && cmd.nb_outs == 0;

#define declare_var()													\
	int i_cmd; int _delay;												\
	int i_time_cmd;														\
	int	i, j, k, l, m;													\
	bool _is_flush_cmd, _time_cmd, _hasCompleted, _is_external, _inactive;\
	command_list list;													\

#define allocate() 													\
	i = 0;																\
	do																	\
	::i < list.size ->													\
		inactive(list.cmd[i])											\
		if																\
		::_inactive -> i_cmd = i; break;								\
		::else -> skip;													\
		fi;																\
		i++;															\
	::else -> i_cmd = list.size; list.size++; break;					\
	od;																	\
	cmd_copy(list.cmd[i_cmd], cmd)										\
	list.dep[i_cmd].tasks[0] = i_cmd; 									\
	list.dep[i_cmd].depth = 1;

#define cmd_list_push()												\
	allocate()															\
	j = 0;																\
	do																	\
	::j < cmd.nb_ins ->													\
		k = 0;															\
		do																\
		:: k < list.size ->												\
			l = 0;														\
			do															\
			:: l < list.cmd[k].nb_outs ->								\
				if 														\
				:: list.cmd[k].outs[l].id == cmd.ins[j].id -> m = 0;	\
					do													\
					:: m < list.dep[k].depth ->							\
						list.dep[i_cmd].tasks[list.dep[i_cmd].depth] = list.dep[k].tasks[m];\
						list.dep[i_cmd].depth++;						\
						m++;											\
					:: else -> break;									\
					od;													\
				:: else -> break;										\
				fi;														\
				l++;													\
			:: else -> break;											\
			od;															\
			k++;														\
		:: else -> break;												\
		od;																\
		j++;															\
	:: else -> break;													\
	od;

#define free_cmd_list()												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		free_cmd(list.cmd[k])											\
		i++;															\
	:: else -> list.dep[i_cmd].depth = 0; break;						\
	od;

#define is_flush_cmd()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		is_external(cmd.outs[i].loc)									\
		if 																\
		:: !_is_external -> _is_flush_cmd = false; break;				\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> _is_flush_cmd = true; break;								\
	od;


#define prog_pipeline()												\
	nb_ins = 0; nb_outs = 0;											\
	i_ins = 0; i_outs = 0;												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_ins ->									\
			is_external(list.cmd[k].ins[j].loc)							\
			if															\
			:: _is_external -> 											\
				fetch[nb_ins] = 0;										\
				data_copy(ins[nb_ins], list.cmd[k].ins[j]) nb_ins++;	\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_outs ->									\
			is_external(list.cmd[k].outs[j].loc)						\
			if															\
			:: _is_external -> 											\
				store[nb_outs] = 0;										\
				data_copy(outs[nb_outs], list.cmd[k].outs[j]) nb_outs++;\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		i++;															\
	:: else -> break;													\
	od;

#define hasCompleted()													\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		:: fetch[i] < ins[i].size -> _hasCompleted = false; break;		\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> _hasCompleted = true; break;								\
	od;																	\
	i = 0;																\
	do																	\
	:: i < nb_outs && _hasCompleted ->									\
		if																\
		:: store[i] < outs[i].size -> _hasCompleted = false; break; 	\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> break;													\
	od;

#define checkIns()														\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		::fetch[i] >= ins[i].size -> nb_ins--;							\
			data_copy(ins[i], ins[nb_ins])								\
			fetch[i] = fetch[nb_ins];									\
			fetch[nb_ins] = 0;											\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> i_ins = 0; break;										\
	od;

#define checkOuts()														\
	i = 0;																\
	do																	\
	:: i < nb_outs ->													\
		if																\
		::store[i] >= outs[i].size -> nb_outs--;						\
			data_copy(outs[i], outs[nb_outs])							\
			store[i] = store[nb_outs];									\
			store[nb_outs] = 0;											\
		::else -> skip;													\
		fi;																\
		i++;															\
	:: else -> i_outs = 0; break;										\
	od;

#define setOutputs()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		cmd.outs[i].size = cmd.ins[0].size;	i++;						\
	:: else -> break;													\
	od;

#define time_cmd(cmd)												\
	if																	\
	::cmd.fct == Plt_P0_C0_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C0_P7_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P0_C1_P7_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C0_P7_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P0_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P1_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P2_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P3_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P4_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P5_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P6_ID -> _time_cmd = 4;					\
	::cmd.fct == Plt_P1_C1_P7_ID -> _time_cmd = 4;					\
	fi;
#define time_cmds()														\
	time_cmd(list.cmd[ list.dep[i_cmd].tasks[0] ])						\
	l = _time_cmd;														\
	i = 1;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		l = _time_cmd;													\
		time_cmd(list.cmd[k])											\
		i++;															\
		if																\
		::_time_cmd < l -> l = _time_cmd;								\
		:: else -> skip;												\
		fi;																\
	:: else -> break;													\
	od;																	\
	_delay = (burst/l)*(main_freq/freq);

bool plt_p0_c0_idle = true;

active proctype processor_Plt_P0_C0(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
do
	::send[Plt_P0_C0_ID]?cmd -> plt_p0_c0_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P0_C0_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P0_C0_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p0_c0_idle = true;
	od;
};
}

bool plt_p1_c0_idle = true;

active proctype processor_Plt_P1_C0(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
do
	::send[Plt_P1_C0_ID]?cmd -> plt_p1_c0_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P1_C0_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P1_C0_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p1_c0_idle = true;
	od;
};
}

bool plt_p0_c1_idle = true;

active proctype processor_Plt_P0_C1(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
do
	::send[Plt_P0_C1_ID]?cmd -> plt_p0_c1_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P0_C1_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P0_C1_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p0_c1_idle = true;
	od;
};
}

bool plt_p1_c1_idle = true;

active proctype processor_Plt_P1_C1(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
do
	::send[Plt_P1_C1_ID]?cmd -> plt_p1_c1_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[Plt_P1_C1_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[Plt_P1_C1_ID]!outs[i_outs]; free_cmd_list()
		fi;
       plt_p1_c1_idle = true;
	od;
};
}

chan PT0_T2 = [0] of {data}
chan PD1_T0 = [0] of {data}
chan PT3_T2 = [0] of {data}
chan PT4_T3 = [0] of {data}
chan PT5_T4 = [0] of {data}
chan PT6_T9 = [0] of {data}
chan PT6_T5 = [0] of {data}
chan PD7_T8 = [0] of {data}
chan PD7_T6 = [0] of {data}
chan PD10_T11 = [0] of {data}
chan PD10_T5 = [0] of {data}
chan PT11_T12 = [0] of {data}
chan PT13_T12 = [0] of {data}
chan PT14_T13 = [0] of {data}
chan PT15_T14 = [0] of {data}
chan PT16_T15 = [0] of {data}
chan PD17_T18 = [0] of {data}
chan PD17_T16 = [0] of {data}
chan PD19_T18 = [0] of {data}

bool d19_end = false;

active proctype input_D19(){ atomic {
	qualityLoss + 1;
	data in;
	in.id = 1;
	in.size = 256;
	gd
	::f.D19_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D19_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D19_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD19_T18!in;
	d19_end = true;
};
}


bool d17_end = false;

active proctype input_D17(){ atomic {
	data in;
	in.id = 2;
	in.size = 512;
	gd
	::f.D17_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D17_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D17_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD17_T18!in;

	PD17_T16!in;
	d17_end = true;
};
}


bool t18_end = false;

active proctype T18(){ atomic {
	command cmd;

	byte proc = Plt_P1_C1_P3_ID;
	byte pu = Plt_P1_C1_ID;
	data PD17_T18_d;
	data PD19_T18_d;
	PD17_T18?PD17_T18_d;
	PD19_T18?PD19_T18_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD17_T18_d)
	data_copy(cmd.ins[1], PD19_T18_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t18_end = true;
};
}


bool t16_end = false;

active proctype T16(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P0_C1_P1_ID;
	byte pu = Plt_P0_C1_ID;
	data PD17_T16_d;
	data PT16_T15_d;
	PD17_T16?PD17_T16_d;
	PT16_T15_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD17_T16_d)
	cmd.nb_ins = 1;
	PT16_T15_d.id = 3;PT16_T15_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT16_T15_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT16_T15_d, cmd.outs[0])
	PT16_T15!PT16_T15_d;
	t16_end = true;
};
}


bool t15_end = false;

active proctype T15(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P0_C1_P1_ID;
	byte pu = Plt_P0_C1_ID;
	data PT16_T15_d;
	data PT15_T14_d;
	PT16_T15?PT16_T15_d;
	PT15_T14_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT16_T15_d)
	cmd.nb_ins = 1;
	PT15_T14_d.id = 4;PT15_T14_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT15_T14_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT15_T14_d, cmd.outs[0])
	PT15_T14!PT15_T14_d;
	t15_end = true;
};
}


bool t14_end = false;

active proctype T14(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P1_ID;
	byte pu = Plt_P0_C0_ID;
	data PT15_T14_d;
	data PT14_T13_d;
	PT15_T14?PT15_T14_d;
	PT14_T13_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT15_T14_d)
	cmd.nb_ins = 1;
	PT14_T13_d.id = 5;PT14_T13_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT14_T13_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT14_T13_d, cmd.outs[0])
	PT14_T13!PT14_T13_d;
	t14_end = true;
};
}


bool t13_end = false;

active proctype T13(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P0_C1_P1_ID;
	byte pu = Plt_P0_C1_ID;
	data PT14_T13_d;
	data PT13_T12_d;
	PT14_T13?PT14_T13_d;
	PT13_T12_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT14_T13_d)
	cmd.nb_ins = 1;
	PT13_T12_d.id = 6;PT13_T12_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT13_T12_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT13_T12_d, cmd.outs[0])
	PT13_T12!PT13_T12_d;
	t13_end = true;
};
}


bool d10_end = false;

active proctype input_D10(){ atomic {
	qualityLoss + 1;
	data in;
	in.id = 7;
	in.size = 1024;
	gd
	::f.D10_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D10_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D10_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD10_T11!in;

	PD10_T5!in;
	d10_end = true;
};
}


bool t11_end = false;

active proctype T11(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P1_C0_P5_ID;
	byte pu = Plt_P1_C0_ID;
	data PD10_T11_d;
	data PT11_T12_d;
	PD10_T11?PD10_T11_d;
	PT11_T12_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD10_T11_d)
	cmd.nb_ins = 1;
	PT11_T12_d.id = 8;PT11_T12_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT11_T12_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT11_T12_d, cmd.outs[0])
	PT11_T12!PT11_T12_d;
	t11_end = true;
};
}


bool t12_end = false;

active proctype T12(){ atomic {
	qualityLoss + 2;
	command cmd;

	byte proc = Plt_P1_C1_P3_ID;
	byte pu = Plt_P1_C1_ID;
	data PT11_T12_d;
	data PT13_T12_d;
	PT11_T12?PT11_T12_d;
	PT13_T12?PT13_T12_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT11_T12_d)
	data_copy(cmd.ins[1], PT13_T12_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t12_end = true;
};
}


bool d7_end = false;

active proctype input_D7(){ atomic {
	data in;
	in.id = 9;
	in.size = 512;
	gd
	::f.D7_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D7_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D7_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD7_T6!in;

	PD7_T8!in;
	d7_end = true;
};
}


bool t8_end = false;

active proctype T8(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P7_ID;
	byte pu = Plt_P0_C0_ID;
	data PD7_T8_d;
	PD7_T8?PD7_T8_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD7_T8_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t8_end = true;
};
}


bool t6_end = false;

active proctype T6(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C0_P5_ID;
	byte pu = Plt_P0_C0_ID;
	data PD7_T6_d;
	data PT6_T9_d;
	data PT6_T5_d;
	PD7_T6?PD7_T6_d;
	PT6_T9_d.loc = Plt_IntRAM0_ID;
	PT6_T5_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD7_T6_d)
	cmd.nb_ins = 1;
	PT6_T9_d.id = 10;PT6_T9_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT6_T9_d)
	PT6_T5_d.id = 11;PT6_T5_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[1], PT6_T5_d)
	cmd.nb_outs = 2;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT6_T9_d, cmd.outs[0])
	data_copy(PT6_T5_d, cmd.outs[1])
	PT6_T9!PT6_T9_d;
	PT6_T5!PT6_T5_d;
	t6_end = true;
};
}


bool t9_end = false;

active proctype T9(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P1_C1_P0_ID;
	byte pu = Plt_P1_C1_ID;
	data PT6_T9_d;
	PT6_T9?PT6_T9_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT6_T9_d)
	cmd.nb_ins = 1;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t9_end = true;
};
}


bool t5_end = false;

active proctype T5(){ atomic {
	command cmd;

	byte proc = Plt_P1_C1_P6_ID;
	byte pu = Plt_P1_C1_ID;
	data PT6_T5_d;
	data PD10_T5_d;
	data PT5_T4_d;
	PT6_T5?PT6_T5_d;
	PD10_T5?PD10_T5_d;
	PT5_T4_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT6_T5_d)
	data_copy(cmd.ins[1], PD10_T5_d)
	cmd.nb_ins = 2;
	PT5_T4_d.id = 12;PT5_T4_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT5_T4_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT5_T4_d, cmd.outs[0])
	PT5_T4!PT5_T4_d;
	t5_end = true;
};
}


bool t4_end = false;

active proctype T4(){ atomic {
	qualityLoss + 1;
	command cmd;

	byte proc = Plt_P0_C1_P1_ID;
	byte pu = Plt_P0_C1_ID;
	data PT5_T4_d;
	data PT4_T3_d;
	PT5_T4?PT5_T4_d;
	PT4_T3_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT5_T4_d)
	cmd.nb_ins = 1;
	PT4_T3_d.id = 13;PT4_T3_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT4_T3_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT4_T3_d, cmd.outs[0])
	PT4_T3!PT4_T3_d;
	t4_end = true;
};
}


bool t3_end = false;

active proctype T3(){ atomic {
	command cmd;

	byte proc = Plt_P1_C0_P5_ID;
	byte pu = Plt_P1_C0_ID;
	data PT4_T3_d;
	data PT3_T2_d;
	PT4_T3?PT4_T3_d;
	PT3_T2_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT4_T3_d)
	cmd.nb_ins = 1;
	PT3_T2_d.id = 14;PT3_T2_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT3_T2_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT3_T2_d, cmd.outs[0])
	PT3_T2!PT3_T2_d;
	t3_end = true;
};
}


bool d1_end = false;

active proctype input_D1(){ atomic {
	qualityLoss + 1;
	data in;
	in.id = 15;
	in.size = 1024;
	gd
	::f.D1_On_Plt_IntROM0-> 
		in.loc = Plt_IntROM0_ID;
	::f.D1_On_Plt_ExtROM0-> 
		in.loc = Plt_ExtROM0_ID;
	::f.D1_On_Plt_IntRAM0-> 
		in.loc = Plt_IntRAM0_ID;
	dg;

	PD1_T0!in;
	d1_end = true;
};
}


bool t0_end = false;

active proctype T0(){ atomic {
	command cmd;

	byte proc = Plt_P0_C0_P1_ID;
	byte pu = Plt_P0_C0_ID;
	data PD1_T0_d;
	data PT0_T2_d;
	PD1_T0?PD1_T0_d;
	PT0_T2_d.loc = Plt_IntRAM0_ID;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PD1_T0_d)
	cmd.nb_ins = 1;
	PT0_T2_d.id = 16;PT0_T2_d.loc = Plt_IntRAM0_ID;
	data_copy(cmd.outs[0], PT0_T2_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(PT0_T2_d, cmd.outs[0])
	PT0_T2!PT0_T2_d;
	t0_end = true;
};
}


bool t2_end = false;

active proctype T2(){ atomic {
	command cmd;

	byte proc = Plt_P1_C1_P3_ID;
	byte pu = Plt_P1_C1_ID;
	data PT0_T2_d;
	data PT3_T2_d;
	PT0_T2?PT0_T2_d;
	PT3_T2?PT3_T2_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], PT0_T2_d)
	data_copy(cmd.ins[1], PT3_T2_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	t2_end = true;
};
}

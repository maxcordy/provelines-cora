check <> (d1_end && ta_end && tb_end && d2_end && td_end && tc_end && soc_rom_idle && soc_ram_idle && soc_dcu_idle && soc_gpu_1_idle && soc_gpu_2_idle) within 84000000
typedef features{
bool DesignSpaceVariability;
bool ApplicationVariability;
bool TA;
bool TB;
bool D2_size;
bool D2_size_256;
bool D2_size_512;
bool D2_size_1024;
bool P1_To;
bool P1_To_TA;
bool P1_To_TB;
bool P2_From;
bool P2_From_TA;
bool P2_From_TB;
bool PlatformVariability;
bool SoC_ROM;
bool SoC_ROM_frequency;
bool SoC_ROM_frequency_100;
bool SoC_ROM_frequency_200;
bool SoC_RAM;
bool SoC_RAM_capacity;
bool SoC_RAM_capacity_512;
bool SoC_RAM_capacity_1024;
bool SoC_RAM_capacity_2048;
bool SoC_RAM_frequency;
bool SoC_RAM_frequency_100;
bool SoC_RAM_frequency_200;
bool SoC_GPU;
bool SoC_GPU_frequency;
bool SoC_GPU_frequency_100;
bool SoC_GPU_frequency_200;
bool SoC_GPU_1;
bool SoC_GPU_1_A;
bool SoC_GPU_1_R0;
bool SoC_GPU_1_B;
bool SoC_GPU_2;
bool SoC_GPU_2_D;
bool DeploymentVariability;
bool D2_On;
bool D2_On_SoC_ROM;
bool D2_On_SoC_RAM;
bool D1_On;
bool D1_On_SoC_ROM;
bool D1_On_SoC_RAM;
bool P1_On;
bool P1_On_SoC_ROM;
bool P1_On_SoC_RAM;
bool P4_On;
bool P4_On_SoC_DCU_R1;
bool P4_On_SoC_RAM;
bool P2_On;
bool P2_On_SoC_DCU_R0;
bool P2_On_SoC_RAM;
bool P3_On;
bool P3_On_SoC_ROM;
bool P3_On_SoC_RAM;
bool TA_On;
bool TA_On_SoC_DCU_A;
bool TA_On_SoC_GPU_1_A;
bool TC_On;
bool TC_On_SoC_DCU_C;
bool TB_On;
bool TB_On_SoC_GPU_1_B;
bool TD_On;
bool TD_On_SoC_DCU_D;
bool TD_On_SoC_GPU_2_D
}features f;
#define main_freq 200
#define burst 512

#define NB_MEMORY 5
#define NULL_MEMORY -1
#define SoC_ROM_ID 0
#define SoC_RAM_ID 1
#define SoC_GPU_1_R0_ID 2
#define SoC_DCU_R0_ID 3
#define SoC_DCU_R1_ID 4


#define NB_PROCESSOR 6
#define NULL_PROCESSOR -1
#define SoC_GPU_1_A_ID 0
#define SoC_GPU_1_B_ID 1
#define SoC_GPU_2_D_ID 2
#define SoC_DCU_A_ID 3
#define SoC_DCU_D_ID 4
#define SoC_DCU_C_ID 5

#define NB_PU 3
#define NULL_PU -1
#define SoC_DCU_ID 0
#define SoC_GPU_1_ID 1
#define SoC_GPU_2_ID 2
typedef data{
	byte id;
	byte loc;
	int size
}

#define MAX_NB_INS 2
#define MAX_NB_OUTS 1

typedef command{
	byte fct;
	data ins[MAX_NB_INS];
	byte nb_ins;
	data outs[MAX_NB_OUTS];
	byte nb_outs
}

#define NB_TASK 4

#define LIST_LENGTH NB_TASK

typedef dependency{
	byte tasks[NB_TASK];
	byte depth
}

typedef command_list{
	command cmd[LIST_LENGTH];
	dependency dep[LIST_LENGTH];
	int size
}

chan transfer[NB_MEMORY] = [0] of {data};
chan send[NB_PU] = [0] of {command};
chan recv[NB_PU] = [0] of {command};



bool soc_rom_idle = true; 

active proctype storage_SoC_ROM(){ atomic {
	int freq;
	int bpc = 2;
	int latency =0;
	int _delay = 0;
	data in;

	gd
	::f.SoC_ROM_frequency_100 -> freq = 100;
	::f.SoC_ROM_frequency_200 -> freq = 200;
	dg;

	_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);

	do
	::transfer[SoC_ROM_ID]?in -> soc_rom_idle = false; 
		wait(_delay) then skip;
		soc_rom_idle = true; 
	od;
};
}

bool soc_ram_idle = true; 

active proctype storage_SoC_RAM(){ 
   gd
      :: f.SoC_RAM ->
	int freq;
	int bpc = 8;
	int latency =0;
	int _delay = 0;
	data in;
	gd
	  :: f.SoC_RAM_frequency_100 -> freq = 100;
	  :: f.SoC_RAM_frequency_200 -> freq = 200;
	dg;
	_delay = (latency*main_freq/freq)
	  +(burst/bpc*main_freq/freq);
	do
	  :: transfer[SoC_RAM_ID]?in 
	    -> soc_ram_idle = false; 
	    wait(_delay) then skip;
	    soc_ram_idle = true; 
	od;
   dg;
}
#define data_copy(dst, src) dst.id = src.id; dst.loc = src.loc; dst.size = src.size;

#define cmd_copy(dst, src) dst.fct = src.fct; dst.nb_ins = src.nb_ins; dst.nb_outs = src.nb_outs; \
	j = 0;																\
	do																	\
	:: j < src.nb_ins -> data_copy(dst.ins[j], src.ins[j]) j++			\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < src.nb_outs -> data_copy(dst.outs[j], src.outs[j]) j++		\
	:: else -> break;													\
	od;

#define free_data(data) data.id = 0; data.loc = 0; data.size = 0;

#define free_cmd(cmd)  \
	j = 0;																\
	do																	\
	:: j < cmd.nb_ins -> free_data(cmd.ins[j]) j++						\
	:: else -> break;													\
	od;																	\
	j = 0;																\
	do																	\
	:: j < cmd.nb_outs -> free_data(cmd.outs[j]) j++					\
	:: else -> break;													\
	od;										\
	cmd.fct = 0; cmd.nb_ins = 0; cmd.nb_outs = 0;
	
#define is_external(id)	_is_external = id == SoC_ROM_ID || id == SoC_RAM_ID;
#define inactive(cmd) _inactive = cmd.fct == 0 && cmd.nb_ins == 0 && cmd.nb_outs == 0;

#define declare_var()													\
	int i_cmd; int _delay;												\
	int i_time_cmd;														\
	int	i, j, k, l, m;													\
	bool _is_flush_cmd, _time_cmd, _hasCompleted, _is_external, _inactive;\
	command_list list;													\

#define allocate() 													\
	i = 0;																\
	do																	\
	::i < list.size ->													\
		inactive(list.cmd[i])											\
		if																\
		::_inactive -> i_cmd = i; break;								\
		::else -> skip;													\
		fi;																\
		i++;															\
	::else -> i_cmd = list.size; list.size++; break;					\
	od;																	\
	cmd_copy(list.cmd[i_cmd], cmd)										\
	list.dep[i_cmd].tasks[0] = i_cmd; 									\
	list.dep[i_cmd].depth = 1;

#define cmd_list_push()												\
	allocate()															\
	j = 0;																\
	do																	\
	::j < cmd.nb_ins ->													\
		k = 0;															\
		do																\
		:: k < list.size ->												\
			l = 0;														\
			do															\
			:: l < list.cmd[k].nb_outs ->								\
				if 														\
				:: list.cmd[k].outs[l].id == cmd.ins[j].id -> m = 0;	\
					do													\
					:: m < list.dep[k].depth ->							\
						list.dep[i_cmd].tasks[list.dep[i_cmd].depth] = list.dep[k].tasks[m];\
						list.dep[i_cmd].depth++;						\
						m++;											\
					:: else -> break;									\
					od;													\
				:: else -> break;										\
				fi;														\
				l++;													\
			:: else -> break;											\
			od;															\
			k++;														\
		:: else -> break;												\
		od;																\
		j++;															\
	:: else -> break;													\
	od;

#define free_cmd_list()												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		free_cmd(list.cmd[k])											\
		i++;															\
	:: else -> list.dep[i_cmd].depth = 0; break;						\
	od;

#define is_flush_cmd()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		is_external(cmd.outs[i].loc)									\
		if 																\
		:: !_is_external -> _is_flush_cmd = false; break;				\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> _is_flush_cmd = true; break;								\
	od;


#define prog_pipeline()												\
	nb_ins = 0; nb_outs = 0;											\
	i_ins = 0; i_outs = 0;												\
	i = 0;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_ins ->									\
			is_external(list.cmd[k].ins[j].loc)							\
			if															\
			:: _is_external -> 											\
				fetch[nb_ins] = 0;										\
				data_copy(ins[nb_ins], list.cmd[k].ins[j]) nb_ins++;	\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		j = 0;															\
		do																\
		:: j < list.cmd[k].nb_outs ->									\
			is_external(list.cmd[k].outs[j].loc)						\
			if															\
			:: _is_external -> 											\
				store[nb_outs] = 0;										\
				data_copy(outs[nb_outs], list.cmd[k].outs[j]) nb_outs++;\
			:: else -> skip;											\
			fi;															\
			j++;														\
		:: else -> break;												\
		od;																\
		i++;															\
	:: else -> break;													\
	od;

#define hasCompleted()													\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		:: fetch[i] < ins[i].size -> _hasCompleted = false; break;		\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> _hasCompleted = true; break;								\
	od;																	\
	i = 0;																\
	do																	\
	:: i < nb_outs && _hasCompleted ->									\
		if																\
		:: store[i] < outs[i].size -> _hasCompleted = false; break; 	\
		:: else -> skip; 												\
		fi;																\
		i++;															\
	:: else -> break;													\
	od;

#define checkIns()														\
	i = 0;																\
	do																	\
	:: i < nb_ins ->													\
		if																\
		::fetch[i] >= ins[i].size -> nb_ins--;							\
			data_copy(ins[i], ins[nb_ins])								\
			fetch[i] = fetch[nb_ins];									\
			fetch[nb_ins] = 0;											\
		:: else -> skip;												\
		fi;																\
		i++;															\
	:: else -> i_ins = 0; break;										\
	od;

#define checkOuts()														\
	i = 0;																\
	do																	\
	:: i < nb_outs ->													\
		if																\
		::store[i] >= outs[i].size -> nb_outs--;						\
			data_copy(outs[i], outs[nb_outs])							\
			store[i] = store[nb_outs];									\
			store[nb_outs] = 0;											\
		::else -> skip;													\
		fi;																\
		i++;															\
	:: else -> i_outs = 0; break;										\
	od;

#define setOutputs()													\
	i = 0;																\
	do																	\
	:: i < cmd.nb_outs ->												\
		cmd.outs[i].size = cmd.ins[0].size;	i++;						\
	:: else -> break;													\
	od;

#define time_cmd(cmd)												\
	if																	\
	::cmd.fct == SoC_GPU_1_A_ID -> _time_cmd = 8;					\
	::cmd.fct == SoC_GPU_1_B_ID -> _time_cmd = 2;					\
	::cmd.fct == SoC_GPU_2_D_ID -> _time_cmd = 16;					\
	::cmd.fct == SoC_DCU_A_ID -> _time_cmd = 4;					\
	::cmd.fct == SoC_DCU_D_ID -> _time_cmd = 4;					\
	::cmd.fct == SoC_DCU_C_ID -> _time_cmd = 8;					\
	fi;
#define time_cmds()														\
	time_cmd(list.cmd[ list.dep[i_cmd].tasks[0] ])						\
	l = _time_cmd;														\
	i = 1;																\
	do																	\
	:: i < list.dep[i_cmd].depth ->										\
		k = list.dep[i_cmd].tasks[i];									\
		l = _time_cmd;													\
		time_cmd(list.cmd[k])											\
		i++;															\
		if																\
		::_time_cmd < l -> l = _time_cmd;								\
		:: else -> skip;												\
		fi;																\
	:: else -> break;													\
	od;																	\
	_delay = (burst/l)*(main_freq/freq);

bool soc_dcu_idle = true;

active proctype processor_SoC_DCU(){ atomic {
	int freq = 100;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()

	freq = 100;
	do
	::send[SoC_DCU_ID]?cmd -> soc_dcu_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[SoC_DCU_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[SoC_DCU_ID]!outs[i_outs]; free_cmd_list()
		fi;
       soc_dcu_idle = true;
	od;
};
}

bool soc_gpu_1_idle = true;

active proctype processor_SoC_GPU_1(){ atomic {
gd
::f.SoC_GPU_1 ->
	int freq;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()


	gd
	::f.SoC_GPU_frequency_100 -> freq = 100;
	::f.SoC_GPU_frequency_200 -> freq = 200;
	dg;
	do
	::send[SoC_GPU_1_ID]?cmd -> soc_gpu_1_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[SoC_GPU_1_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[SoC_GPU_1_ID]!outs[i_outs]; free_cmd_list()
		fi;
       soc_gpu_1_idle = true;
	od;
::else -> skip;
dg;
};
}

bool soc_gpu_2_idle = true;

active proctype processor_SoC_GPU_2(){ atomic {
gd
::f.SoC_GPU_2 ->
	int freq;

	command cmd;
	data ins[2]; int fetch[2]; byte nb_ins = 0; byte i_ins;
	data outs[2]; int store[2]; byte nb_outs = 0; byte i_outs;


	declare_var()


	gd
	::f.SoC_GPU_frequency_100 -> freq = 100;
	::f.SoC_GPU_frequency_200 -> freq = 200;
	dg;
	do
	::send[SoC_GPU_2_ID]?cmd -> soc_gpu_2_idle = false;
		setOutputs() cmd_list_push() is_flush_cmd()
		if
		::!_is_flush_cmd -> recv[SoC_GPU_2_ID]!cmd;
		::else -> prog_pipeline() hasCompleted()
			do
			:: true -> 

				do 
				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];
					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;
				::i_ins == nb_ins -> checkIns() break;
				od;
				
				time_cmds()
				wait(_delay) then skip;
				hasCompleted()
				
				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
				
				do
				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];
					store[i_outs] = store[i_outs] + burst; i_outs++;
				::i_outs == nb_outs -> checkOuts() break;
				od;

				hasCompleted()

				if
				:: _hasCompleted -> break;
				:: else -> skip;
				fi;
			od;	
			recv[SoC_GPU_2_ID]!outs[i_outs]; free_cmd_list()
		fi;
       soc_gpu_2_idle = true;
	od;
::else -> skip;
dg;
};
}

chan P1 = [0] of {data}
chan P2 = [0] of {data}
chan P3 = [0] of {data}
chan P4 = [0] of {data}

bool d2_end = false;

active proctype input_D2(){ atomic {
	data in;
	in.id = 1;
	gd
	::f.D2_size_256-> 
qualityLoss + 2; in.size = 256;
	::f.D2_size_512-> 
qualityLoss + 1; in.size = 512;
	::f.D2_size_1024->  in.size = 1024;
	dg;
	gd
	::f.D2_On_SoC_ROM-> 
		in.loc = SoC_ROM_ID;
	::f.D2_On_SoC_RAM-> 
		in.loc = SoC_RAM_ID;
	dg;

	P3!in;
	d2_end = true;
};
}


bool td_end = false;

active proctype TD(){ atomic {
	command cmd;

	byte proc = 0;
	byte pu = 0;
	data P3_d;
	data P4_d;
	P3?P3_d;
	gd
	::f.TD_On_SoC_DCU_D -> proc = SoC_DCU_D_ID; pu = SoC_DCU_ID;
	::f.TD_On_SoC_GPU_2_D -> proc = SoC_GPU_2_D_ID; pu = SoC_GPU_2_ID;
	dg;
	gd
	::f.P4_On_SoC_DCU_R1 -> P4_d.loc = SoC_DCU_R1_ID;
	::f.P4_On_SoC_RAM -> P4_d.loc = SoC_RAM_ID;
	dg;
	cmd.fct = proc;
	data_copy(cmd.ins[0], P3_d)
	cmd.nb_ins = 1;
	P4_d.id = 2;
	data_copy(cmd.outs[0], P4_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(P4_d, cmd.outs[0])
	P4!P4_d;
	td_end = true;
};
}


bool d1_end = false;

active proctype input_D1(){ atomic {
	data in;
	in.id = 3;
	in.size = 512;
	gd
	::f.D1_On_SoC_ROM-> 
		in.loc = SoC_ROM_ID;
	::f.D1_On_SoC_RAM-> 
		in.loc = SoC_RAM_ID;
	dg;

	P1!in;
	d1_end = true;
};
}


bool tb_end = false;

active proctype TB(){ atomic {
gd::f.TB->

	command cmd;

	byte proc = SoC_GPU_1_B_ID;
	byte pu = SoC_GPU_1_ID;
	data P1_d;
	data P2_d;
	P1?P1_d;
	gd
	::f.P2_On_SoC_DCU_R0 -> P2_d.loc = SoC_DCU_R0_ID;
	::f.P2_On_SoC_RAM -> P2_d.loc = SoC_RAM_ID;
	dg;
	cmd.fct = proc;
	data_copy(cmd.ins[0], P1_d)
	cmd.nb_ins = 1;
	P2_d.id = 4;
	data_copy(cmd.outs[0], P2_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(P2_d, cmd.outs[0])
	P2!P2_d;
::else -> skip;
dg;
	tb_end = true;
};
}


bool ta_end = false;

active proctype TA(){ atomic {
gd::f.TA->

	qualityLoss + 2;
	command cmd;

	byte proc = 0;
	byte pu = 0;
	data P1_d;
	data P2_d;
	P1?P1_d;
	gd
	::f.TA_On_SoC_DCU_A -> proc = SoC_DCU_A_ID; pu = SoC_DCU_ID;
	::f.TA_On_SoC_GPU_1_A -> proc = SoC_GPU_1_A_ID; pu = SoC_GPU_1_ID;
	dg;
	gd
	::f.P2_On_SoC_DCU_R0 -> P2_d.loc = SoC_DCU_R0_ID;
	::f.P2_On_SoC_RAM -> P2_d.loc = SoC_RAM_ID;
	dg;
	cmd.fct = proc;
	data_copy(cmd.ins[0], P1_d)
	cmd.nb_ins = 1;
	P2_d.id = 5;
	data_copy(cmd.outs[0], P2_d)
	cmd.nb_outs = 1;
	send[pu]!cmd;
	recv[pu]?cmd;
	data_copy(P2_d, cmd.outs[0])
	P2!P2_d;
::else -> skip;
dg;
	ta_end = true;
};
}


bool tc_end = false;

active proctype TC(){ atomic {
	command cmd;

	byte proc = SoC_DCU_C_ID;
	byte pu = SoC_DCU_ID;
	data P2_d;
	data P4_d;
	P2?P2_d;
	P4?P4_d;
	cmd.fct = proc;
	data_copy(cmd.ins[0], P2_d)
	data_copy(cmd.ins[1], P4_d)
	cmd.nb_ins = 2;
	cmd.nb_outs = 0;
	send[pu]!cmd;
	recv[pu]?cmd;
	tc_end = true;
};
}

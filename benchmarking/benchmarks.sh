echo "Start Benchmarks"
echo "----------------"

if [ "$1" = '' ]; then
	TS=$(date +%Y%m%d)
else
	TS=$1
fi;

PML=../test/0-Models.fpta.pml
TVL=../test/0-Models.fpta.tvl
CLAUSES=../test/0-Models.fpta.clauses
MAPPING=../test/0-Models.fpta.mapping
OUT=results-$TS.txt

php run.php $PML $TVL $CLAUSES $MAPPING \
		-dac \
		| tee $OUT


echo "End"
echo "----------------"
